
##print('Second param:'+sys.argv[2]+'#')
import io
import os
import sys, json
import base64
from typing import IO
from PIL import Image
import cv2
import numpy as np
import PIL.Image

import pytesseract
from pytesseract import image_to_string


import re 
import datetime
#Read data from stdin
def read_in():
    lines = sys.stdin.readlines()
    # Since our input would only be having one line, parse our JSON data from that
    return lines
#def save(encoded_data, filename):
 #   nparr = np.fromstring(encoded_data.decode('base64'), np.uint8)
  #  img = cv2.imdecode(nparr, cv2.IMREAD_ANYCOLOR)
   # return cv2.imwrite(filename, img)
def stringToRGB(base64_string):
    imgdata = base64.b64decode(str(base64_string))
    image = Image.open(io.BytesIO(imgdata))
    return cv2.cvtColor(np.array(image), cv2.COLOR_BGR2RGB)
def search_string_in_file(file_name, string_to_search1,string_to_search2):
    
    line_number = 0
    list_of_results = []
    # Open the file in read only mode
    with open(file_name, 'r') as read_obj:
        # Read all lines in the file one by one
        for line in read_obj:
            # For each line, check if line contains the string
            line_number += 1
            if string_to_search1  in line:
                # If yes, then add the line number & line as a tuple in the list
                #list_of_results.append((line_number, line.rstrip()))
                 list_of_results.append(line.rstrip())
            if string_to_search2 in line:
                #list_of_results.append((line_number, line.rstrip()))
                list_of_results.append(line.rstrip())
    # Return list of tuples containing line numbers and lines where string is found
    return list_of_results
def main():
    #get our data as an array from read_in()
    lines = read_in()

    # Sum  of all the items in the providen array
    #fh = open("imageToSave.png", "wb")
    #fh.write(lines.decode('base64'))
    #fh.close()
    ####################
    #imgdata = base64.b64decode(lines)
    #filename = 'img_received.jpg'  # I assume you have a way of picking unique filenames
    #with open(filename, 'wb') as f:
             #f.write(lines)
    ##########################
    window_name = 'Image'
    image = stringToRGB(lines)
    imS = cv2.resize(image, (960, 700))
    #pytesseract.pytesseract.tesseract_cmd = r"C:\Program Files\Tesseract-OCR\tesseract.exe"
    pytesseract.pytesseract.tesseract_cmd = r"./Tesseract-OCR/tesseract.exe"
    image_to_text = pytesseract.image_to_string(image, lang='eng')
    #f = open("sample_content.txt", "w")
    name = sys.argv[1]
    f = open('%s_content.txt' % name, "w")
    f.write(image_to_text)
    f.close()
    matched_lines = search_string_in_file('%s_content.txt' % name, 'Total', 'A PAYER')
    
#print('\n Lignes trouvées : ', len(matched_lines))
    for elem in matched_lines:
        print("\n")
        print('La ligne trouvée:',elem)
    try:
     os.remove('%s_content.txt' % name)
    except OSError as e:
      print(e)
    else:
      print("File is deleted successfully")
    cv2.imshow(window_name, imS)
    cv2.waitKey(0)  
    cv2.destroyAllWindows()
    
    
    
    

# Start process
if __name__ == '__main__':
    main()

    
