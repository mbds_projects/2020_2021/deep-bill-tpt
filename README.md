#### Deep Bill


### **mekkaoui**
## **taches de la semaine  du lundi 17 Mai au vendredi 17 Mai**
 **mardi et mercredi** (preparation et passage d'un entretien technique de stage)
 **jeudi et vendredi** (architecture de l'application "frontend" et documentation sur la mise en place du serveur python)
 **mardi-vendredi** création d'un serveur python, l'extraction de texte du ticket de caisse et renseignement sur la détection des mots(synonymes)

 lundi 31-05: detection des différents format de date dans un fichier texte.
 mardi et mercredi jeudi 01/02/03-06: injection du script python dans le serveur python. communication entre le serveur nodejs et python.

  **mardi-vendredi** création d'un serveur python, l'extraction de texte du ticket de caisse et renseignement sur la détection des mots(synonymes)

 **mardi et mercredi jeudi 01/02/03-06:** injection du script python dans le serveur python. communication entre le serveur nodejs et python.

  **lundi 07/06:** problème d'installation du système node (serveur et client)sur ma machine. j'ai fait une demande à la bibliothèque pour un ordinateur.
 mardi: j'attends la réponse de mr Mathieu.BARBIERI, je lui ai envoyé un mail hier, pour emprunter un ordinateur.

 du mercredi 09/06 jusqu'à maintenant: appeler le script python sur js.

 16/06: on arrive maintenant à envoyer une image depuis js et la lire en tant qu'image sur python( et extraire le texte). on passe maintenant à l'application de ça sur notre serveur node.js . 

 17/06: quand le client upload son image et clique sur "submit", l'image est traitée par un script python qui fait l'extraction du contenu et le met dans un fichier texte(avec le meme nom de l'image).
je discute demain avec sadio pour voir quelles sont les donner qu'on doit extraire, pour modifier le contenu de la base de données et passer à la gestion des taches par l'assistante. 

18..23 /06 : detection du montant en utilisant les fonctionnalités offertes par nlp et l'insertion de ce montant dans la base de données.

23/06 : préparation d'un document décrivant les différentes taches de l'assistante.

24/06.....01/07 : lister les factures traitées et non traitées pour chaque utilisateur(avec mamane). la génération d'un fichier csv contenant "nom du client, montant de sa facture et la date du demande de remboursement".


## ** information sur le dernier push. (le 10/07)***

** dans le dernier push on a mis l'application web presque terminée (il manque quelques petits détails).

** on a un client qui a : {

               + un espace pour uploader une/plusieurs factures.

               + un espace pour consulter si ses factures sont traitées ou pas encore. 

}

** on a une assistante qui a : {

               + un espace personnel pour uploader ses propres factures.

               + un espace pour uploader les factures à la place des utilisateurs.

               + un espace pour gérer les factures des clients, avec la génération d'un fichier excel
          contenant les informations du client (note de frais).
               
}

** on a un admin qui a : {

               + un espace pour consulter ses propres factures (d'un point de vue où l'admin peut aussi etre un client).
               
               + un espace pour uploader des factures.

               + un espace pour gérer les factures (sauf la génération des notesde frais).

               + un espace pour affecter les roles (à la première inscription, vous etes un utilsateur, et c'est l'admin
            qui peut vous affecter le role "assistante").

}

## ** à faire**

 + compléter les détails manquants dans l'application web.

 + commencer l'application mobile.



### **sadio**

## **taches de la semaine  du lundi 17 Mai au vendredi 17 Mai**
* **Mise en place du back-end**
 * Construire le backend Node JS de l'application
 * Configuration de la base de données MongoDB
 * Définir les differents schémas Mongoose
 * Créer des itinéraires à l'aide d'Express / Node JS pour l'application 
 * Configurer Server.js dans le backend Node / Express.js
 * Utilisation de postman  pour effectuer une requête HTTP et tester les serveur
* **Mise en place du client web React**
 * Créer de application avec React
 * Intégration de React Bootstrap avec l'application React
 * Création de composants React de l'application
 * Implémentation du routeur React
 * Créer un formulaire de réaction avec React Bootstrap pour tester les insertions
 * Soumettre les données des formulaires dans React
 * Afficher la liste de données avec React Axios
 * Modifier, mettre à jour et supprimer des données dans a travers le client React
 * Style de l'application CRUD
## **taches de la semaine  du lundi 24 Mai au vendredi 30 Mai**
 * **mise en place de l'authentification(san token)**
 * Lundi et mardi
 - architecture et implementation
 * **correction des bug** 
 * mercredi
 * **debut de l'authentification avec JWT**
 - architecture et debut implementation
 ## **taches de la semaine  du lundi 31 Mai au vendredi 06 Juin**
 * **finalisation de l'authentification**
 * mardi 1 juin et mercredi 2 juin
 - gestion de l'authentification avec les roles en JWT
 - test de lauthentification
 * jeudi commencer la gestion de l'interface client avec l'envoi des facture sur le serveur
 * vendredi chargement des facture sur le serveur
 - coté client avec react et aaxios
 - coté serveur avec multer

  ## **taches de la semaine  du lundi 07 Juin au vendredi 11 Juin**
 * **Correction de l'authentification avec le token**
 * mardi 8 juin et mercredi 9 juin
 - gestion de l'authentification avec les roles en JWT
 - test final de lauthentification
 * jeudi documentation sur comment charger les images sur le serveur
 * vendredi mise en place du chargement de l'image sur le serveur

  ## **taches de la semaine  du lundi 14 Juin au vendredi 18 Juin**
- **lundi 14/06:** 
- **Documentation sur la Lison des deux serveurs afin de proceder au traitement** 
- **Faire un point avec Nassim** 
- mardi 15/06
- **Echange avec Nassim et essai de mise en place de la liaison**
- mercredi 16/06
- **recuperation et affichage des factures du client**
- jeudi 17/06 test de la liaison python et node

- 18/06..29/06: organisation de l'espace de l'assistante (création des différentes pages nécéssaires). lister les factures traitées et non traitées pour chaque client (avec mekkaoui).

-30/06....: création d'un espace pour que l'assistante puisse uploader les factures à la place du client. 
