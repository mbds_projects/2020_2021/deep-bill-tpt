import React, { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import SideMenu from "./components/SideMenu";
import SideMenu2 from "./components/SideMenu2";
import Header from "./components/Header";
import "./App.css";

import AuthService from "./services/auth.service";

import Login from "./components/login.component";
import Register from "./components/register.component";
import Home from "./components/home.component";
import Profile from "./components/profile.component";
import BoardUser from "./components/board-user.component";
import BoardUserAdmin from "./components/board-user-admin.component";
import BoardAssistant from "./components/board-assistant.component";
import BoardAdmin from "./components/board-admin.component";
import BoardSuperAdmin from "./components/board-super-admin.component";
import CreateFacture from "./components/create-facture.component";
import FList from "./components/facture.component";
import UserList from "./components/user-list.component";
import EditUser from "./components/edit-user.component";
import EditSuperUser from "./components/edit-super-user.component";
import EditFacture from "./components/edit-facture.component";
import EditFactureAssistant from "./components/edit-facture-assistant.component";
import UpdateFacture from "./components/update-facture.component";
import UpdateFactureAssistant from "./components/update-facture-assistant.component";
import DetailFacture from "./components/detail-facture.component";
import DetailFactureAdmin from "./components/detail-facture-admin.component";
import DetailFactureSuperAdmin from "./components/detail-facture-super-admin.component";
import DetailFacturePersonnelle from "./components/detail-facture-personnelle.component";
import DetailFacturePersonnelleAdmin from "./components/detail-facture-personnelle-admin.component";
import DetailFacturePersonnelleSuperAdmin from "./components/detail-facture-personnelle-super-admin.component";
import DetailFacturePersonnelleAssistant from "./components/detail-facture-personnelle-assistant.component";
import DeleteFacture from "./components/delete-facture.component";
import DeleteFactureAdmin from "./components/delete-facture-admin.component";
import DeleteFactureSuperAdmin from "./components/delete-facture-super-admin.component";
import DeleteFactureUser from "./components/delete-facture-user.component";
import DeleteUserFactureAdmin from "./components/delete-user-facture-admin.component";
import DeleteUserFactureSuperAdmin from "./components/delete-user-facture-super-admin.component";
import DeleteFactureAssistant from "./components/delete-facture-assistant.component";
import UsersFactures from "./components/users-facture.component";
import UsersFacturesAdmin from "./components/users-facture-admin.component";
import UsersFacturesSuperAdmin from "./components/users-facture-super-admin.component";
import UsersFacturesPersonnelles from "./components/users-facture-personnelle.component";
import UsersFacturesPersonnellesAdmin from "./components/users-facture-personnelle-admin.component";
import UsersFacturesPersonnellesSuperAdmin from "./components/users-facture-personnelle-super-admin.component";
import UsersFacturesPersonnellesAssistant from "./components/users-facture-personnelle-assistant.component";
import UsersFacturesnontraitees from "./components/users-facture-nontraitee.component";
import UploadUserFacture from "./components/upload-userFacture.component";


class App extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);

    this.state = {
      
    };
  }

  componentDidMount() {
    const user = AuthService.getCurrentUser();
    if (user) {
      this.setState({
    
      });
    }
  }

  logOut() {
    AuthService.logout();
  }

  render() {
    //const { currentUser, showAssistantBoard, showAdminBoard, showUserBord } = this.state;
    //console.log("je suis l'id", currentUser)

    return (
      <>
      
        <SideMenu/>
        <SideMenu2/>
        <Header/>
        
        
        
        <div className="container mt-3">
          <Switch>
            <Route exact path={["/", "/home"]} component={Home} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/profile" component={Profile} />
            <Route path="/user" component={BoardUser} />
            <Route path="/useradmin" component={BoardUserAdmin} />
            <Route path="/facture" component={CreateFacture} />
            <Route path="/userfacture/CreateUserFacture" component={UploadUserFacture} />
            <Route path="/factures" component={FList} />
            <Route path="/test/allUsers" component={UserList} />
            <Route path="/test/edit/:id" component={EditUser} />
            <Route path="/test/update/:id" component={EditUser} />
            <Route path="/test/superupdate/:id" component={EditSuperUser} />
            <Route path="/edit/:id" component={EditFacture} />
            <Route path="/editassistant/:id" component={EditFactureAssistant} />
            <Route path="/update/:id" component={UpdateFacture} />
            <Route path="/updateassistant/:id" component={UpdateFactureAssistant} />
            <Route path="/delete/:id" component={DeleteFacture} />
            <Route path="/deleteadmin/:id" component={DeleteFactureAdmin} />
            <Route path="/deletesuperadmin/:id" component={DeleteFactureSuperAdmin} />
            <Route path="/deleteuser/:id" component={DeleteFactureUser} />
            <Route path="/deleteadminuserfacture/:id" component={DeleteUserFactureAdmin} />
            <Route path="/deletesuperadminuserfacture/:id" component={DeleteUserFactureSuperAdmin} />
            <Route path="/deleteassistant/:id" component={DeleteFactureAssistant} />
            <Route path="/userfacture/:userId" component={UsersFactures} />
            <Route path="/userfactureadmin/:userId" component={UsersFacturesAdmin} />
            <Route path="/userfacturesuperadmin/:userId" component={UsersFacturesSuperAdmin} />
            <Route path="/userfacturepersonnelle/:userId" component={UsersFacturesPersonnelles} />
            <Route path="/userfacturepersonnelleadmin/:userId" component={UsersFacturesPersonnellesAdmin} />
            <Route path="/userfacturepersonnellesuperadmin/:userId" component={UsersFacturesPersonnellesSuperAdmin} />
            <Route path="/userfacturepersonnelleassistant/:userId" component={UsersFacturesPersonnellesAssistant} />
            <Route path="/userfacturenontraitee/:userId" component={UsersFacturesnontraitees} />
            <Route path="/detail/:id" component={DetailFacture} />
            <Route path="/detailadmin/:id" component={DetailFactureAdmin} />
            <Route path="/detailsuperadmin/:id" component={DetailFactureSuperAdmin} />
            <Route path="/detailpersonnel/:id" component={DetailFacturePersonnelle} />
            <Route path="/detailpersonneladmin/:id" component={DetailFacturePersonnelleAdmin} />
            <Route path="/detailpersonnelsuperadmin/:id" component={DetailFacturePersonnelleSuperAdmin} />
            <Route path="/detailpersonnelassistant/:id" component={DetailFacturePersonnelleAssistant} />
            <Route path="/assistant" component={BoardAssistant} />
            <Route path="/admin" component={BoardAdmin} />
            <Route path="/superadmin" component={BoardSuperAdmin} />
          </Switch>
        </div>
      </>
    );
  }
}

export default App;
