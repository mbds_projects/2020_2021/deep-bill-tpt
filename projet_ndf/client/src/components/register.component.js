import React, { Component } from "react";
import Form from "react-validation/build/form";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import { Card } from 'react-bootstrap';
import '../App.css'
import AuthService from "../services/auth.service";

toast.configure()
const notify = () => {
  toast.success("C'est bon. vous etes inscrits!", { position: toast.POSITION.TOP_RIGHT, autoClose: 2000 });
}
const required = value => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

const email = value => {
  if (!isEmail(value)) {
    return (
      <div className="alert alert-danger" role="alert">
        This is not a valid email.
      </div>
    );
  }
};

const vusername = value => {
  if (value.length < 3 || value.length > 20) {
    return (
      <div className="alert alert-danger" role="alert">
        The username must be between 3 and 20 characters.
      </div>
    );
  }
};

const vpassword = value => {
  if (value.length < 6 || value.length > 40) {
    return (
      <div className="alert alert-danger" role="alert">
        The password must be between 6 and 40 characters.
      </div>
    );
  }
};

export default class Register extends Component {
  constructor(props) {
    super(props);
    this.handleRegister = this.handleRegister.bind(this);
    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);

    this.state = {
      username: "",
      email: "",
      password: "",
      successful: false,
      message: ""
    };
  }

  onChangeUsername(e) {
    this.setState({
      username: e.target.value
    });
  }

  onChangeEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value
    });
  }

  handleRegister(e) {
    e.preventDefault();

    this.setState({
      message: "",
      successful: false
    });

    this.form.validateAll();

    if (this.checkBtn.context._errors.length === 0) {
      AuthService.register(
        this.state.username,
        this.state.email,
        this.state.password
      ).then(
        response => {
          this.setState({
            message: response.data.message,
            successful: true

          });

          this.props.history.push('/login')
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          this.setState({
            successful: false,
            message: resMessage
          });
        }
      );
    }

  }

  render() {
    return (
      <>

        <Paper className="paper-register">
          <Card.Img variant="top"
            src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"
            className="profile-img-card"

          />

          <Form
            onSubmit={this.handleRegister}
            ref={c => {
              this.form = c;
            }}
          >
            {!this.state.successful && (
              <div>

                <div className="form-group" style={{ marginTop: '60px', marginLeft: '120px' }}>

                  <TextField
                    variant="filled"
                    color="secondary"
                    type="text"
                    id="username"
                    label="Username"
                    InputLabelProps={{ shrink: true, margin: "dense" }}
                    value={this.state.username}
                    onChange={this.onChangeUsername}
                    validations={[required, vusername]}
                  />
                </div>


                <div className="form-group" style={{ marginTop: '20px', marginLeft: '120px' }}>

                  <TextField
                    variant="filled"
                    color="secondary"
                    type="email"
                    id="Email"
                    label="Email"
                    InputLabelProps={{ shrink: true, margin: "dense" }}
                    value={this.state.email}
                    onChange={this.onChangeEmail}
                    validations={[required, email]}
                  />
                </div>


                <div className="form-group" style={{ marginTop: '20px', marginLeft: '120px' }}>

                  <TextField
                    variant="filled"
                    color="secondary"
                    type="password"
                    id="password"
                    label="Password"
                    InputLabelProps={{ shrink: true, margin: "dense" }}
                    value={this.state.password}
                    onChange={this.onChangePassword}
                    validations={[required, vpassword]}
                  />
                </div>



                <button
                  className="btn btn-primary btn-block"
                  onClick={notify}
                  style={{ width: '100px', marginTop: '40px', marginLeft: '170px' }}>
                  Sign Up
                </button>


              </div>
            )}


            <CheckButton
              style={{ display: "none" }}
              ref={c => {
                this.checkBtn = c;
              }}
            />
          </Form>
        </Paper>

      </>
    );
  }
}
