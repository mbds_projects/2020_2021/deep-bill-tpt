import React, { Component } from "react";
import { Button } from '@material-ui/core';
import AuthService from "../services/auth.service";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import DetailsIcon from '@material-ui/icons/Details';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Typography from '@material-ui/core/Typography';
export default class BoardAssistant extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentUser: undefined,
      showAssistantBoard: false,
    };
  }

  componentDidMount() {
    const user = AuthService.getCurrentUser();
    if (user) {
      this.setState({
        currentUser: user,
        showAssistantBoard: user.roles.includes("ROLE_ASSISTANT"),

      });
    }
  }

  render() {
    const { currentUser, showAssistantBoard } = this.state;
    return (
      <>

        <Button
          startIcon={<ArrowBackIcon />}
          size="small"
          style={{
            fontSize: 8,
            marginLeft: '-10%',
            marginTop: '8%'
          }}
          variant="contained"
          color="secondary"
          href={"../home"}>

        </Button>
        <Typography
          variant="h4"
          color='primary'
          style={{ marginTop: '50px', marginLeft: '20%' }}>
          Espace de gestion des clients
        </Typography>
        <TableContainer component={Paper} className="table-container-home" >

          <Table size="medium" aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="left">La liste des utilisateurs :</TableCell>
                <TableCell align="left">Uploader l'image d'un client :</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow >
                <TableCell align="left">
                  <>
                    {showAssistantBoard && (

                      < Button
                        startIcon={<DetailsIcon />}
                        size="lg"
                        style={{
                          fontSize: 10,
                          marginTop: 50,
                          marginLeft: 50,
                          marginBottom: 20
                        }}
                        variant="contained"
                        color="primary"

                        href={"/user"}
                      >
                        Liste des utilisateurs
                      </Button>



                    )}

                  </>
                </TableCell>
                <TableCell align="left">
                  <>

                    {showAssistantBoard && (

                      <>

                        < Button
                          startIcon={<DetailsIcon />}
                          size="lg"
                          style={{
                            fontSize: 10,
                            marginTop: 50,
                            marginLeft: 50,
                            marginBottom: 20
                          }}
                          variant="contained"
                          color="primary"

                          href={"/userfacture/CreateUserFacture"}
                        >
                          uploader pour client
                        </Button>
                      </>



                    )}
                  </>
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
      </>
    );
  }
}
