import React, { Component } from 'react';
import axios from 'axios';
import { Button } from '@material-ui/core';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import DetailsIcon from '@material-ui/icons/Details';
export default class UserTableRow extends Component {

    constructor(props) {
        super(props);
        this.deleteUser = this.deleteUser.bind(this);
    }

    deleteUser() {
        axios.delete('http://localhost:8080/users/test/delete/' + this.props.obj._id)
            .then((res) => {
                console.log('User successfully deleted!')
            }).catch((error) => {
                console.log(error)
            })

    }

    render() {
        return (
            <TableRow>
                <TableCell component="th" scope="row">
                    {this.props.obj.username}
                </TableCell>
               
                <TableCell component="th" scope="row">
                    < Button
                        startIcon={<DetailsIcon />}
                        size="small"
                        style={{
                            fontSize: 10
                        }}
                        variant="contained"
                        color="primary"
                        href={"/userfacture/" + this.props.obj._id}
                    >
                        Liste des Factures
                    </Button>
                </TableCell>


            </TableRow>
        );
    }
}