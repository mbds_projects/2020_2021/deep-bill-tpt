import React, { Component } from "react";
import axios from 'axios';
import "../App.css";
import { Button } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import UserFactureTable from './UserFactureTable';
import UserFactureTraiteeTable from './UserFactureTraiteeTable';
import { CSVLink } from "react-csv";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import SaveIcon from '@material-ui/icons/Save';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
let restaurationId, commerceId, transportsId, hebergementId, restauration, commerce, transports, hebergement, autre, autreId
var allcategories
export default class UsersFactures extends Component {

  constructor(props) {
    super(props)

    this.onChangeDatedebut = this.onChangeDatedebut.bind(this);
    this.onChangeDatefin = this.onChangeDatefin.bind(this);
    this.state = {
      factures: [],
      list: [],
      filename: '',
      datedebut: '',
      datefin: '',
      listId: [],
      FactureCategorie: [],
      users: [],
      user: ''
    };
  }
  onChangeDatedebut(e) {
    this.setState({
      datedebut: e.target.value
    });
  }

  onChangeDatefin(e) {
    this.setState({
      datefin: e.target.value
    });
  }

  getFactures() {
    axios.get(`http://localhost:8080/factures/userfacture/${this.props.match.params.userId}`)
      .then(res => {
        //console.log("je suis le res",res)

        this.setState({ factures: [...this.state.factures, ...res.data] });
      });
  };
  getUsers() {
    axios.get('http://localhost:8080/users/test/allUsers')
      .then(rest => {

        this.setState({ users: [...rest.data] });

      });

  }

  getCategories(page) {
    axios.get('http://localhost:8080/users/test/allCategorie')
      .then(resp => {

        this.setState({ FactureCategorie: [...resp.data] });
        //console.log("all categorie", this.state.FactureCategorie)
      });
    //console.log("le client", this.props)

  }

  componentDidMount() {
    console.log("componentDidMount")
    this.getFactures(this.state.page);
    this.getUsers();
    this.getCategories();


  }

  DataTable() {
    return this.state.factures.map((res, i) => {
      if (res.traitee === false) {
        return <UserFactureTable obj={res} key={i} />
      }
    });
  }
  DataTable_length() {
    var table = []
    var k = 0

    this.state.factures.map((res, i) => {
      if (res.traitee === false) {

        table[i] = <UserFactureTable obj={res} key={i} />
        k++
      }
    });

    return k
  }
  DataTable_header() {
    //var header = <thead><h3>Le client n'a aucune Facture non traitée</h3></thead>
    var header = <Typography
      variant="h4"
      color='secondary'
    >
      {this.state.user} n'a aucune facture à traiter
    </Typography>
    var header2 = <TableHead>
      <TableRow>
        <TableCell align="left">Nom de la facture</TableCell>
        <TableCell align="left">Date de la demande</TableCell>
        <TableCell align="center">Action</TableCell>
      </TableRow>
    </TableHead>


    if (this.DataTable_length() === 0) {
      return header
    } else {
      return header2
    }

  }

  DataTable2() {
    var table2 = []

    this.state.factures.map((res, i) => {
      if (res.traitee === true) {

        table2[i] = <UserFactureTraiteeTable obj={res} key={i} />

      }
    });
    return table2
  }
  DataTable2_length() {
    var table2 = []
    var k = 0

    this.state.factures.map((res, i) => {
      if (res.traitee === true) {

        table2[i] = <UserFactureTraiteeTable obj={res} key={i} />
        k++
      }
    });

    return k
  }
  DataTable2_header() {
    //var header = <thead><h3>Le client n'a aucune facture traitée</h3></thead>
    var header = <Typography
      variant="h4"
      color='secondary'
    >
      {this.state.user} n'a aucune facture traitée
    </Typography>
    var header2 = <TableHead>
      <TableRow>
        <TableCell align="left">Nom de la facture</TableCell>

        <TableCell align="left">Action</TableCell>
      </TableRow>
    </TableHead>

    if (this.DataTable2_length() === 0) {
      return header
    } else {
      return header2
    }

  }
  padLeadingZeros(num, size) {
    var s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
  }

  ExportCSV() {
    var table = []
    var k = 1
    //console.log("data reçue:", this.state.factures)
    this.state.factures.map((res, i) => {
      if (res.traitee === false) {

        var nowDate = new Date(this.state.factures[i].createdAt);
        var year = nowDate.getFullYear();
        year = year.toString()
        var month = nowDate.getMonth() + 1;
        if (month < 10) {
          month = this.padLeadingZeros(month, 2);
        } else {
          month = month.toString();
        }
        var day = nowDate.getDate();
        day = day.toString()

        var date = year + '-' + month + '-' + day;
        var n, m, id, tva, mht, c
        n = this.state.factures[i].username
        m = this.state.factures[i].montant
        mht = this.state.factures[i].montant_ht
        tva = this.state.factures[i].tva
        id = this.state.factures[i]._id
        c = this.state.factures[i].categories
        this.state.list[i] = [n, m, date, id, tva, mht, c]
        //console.log("list fac :", this.state.list[i])



      }
      //console.log(this.state.list.size)
    });
    var csv = ''
    var client_name = ''

    //console.log("rrr", this.state.list)
    //console.log("length", this.state.list.length)
    //console.log("client name:", client_name)
    var facture = ''
    var factures = [];
    var categoriescsv = [];
    var j = 0, w = 0
    allcategories = this.state.FactureCategorie.map(r => {
      return {
        categorie: r.catégorie,
        _id: r._id
      }
    })
    //console.log("all categories", allcategories)
    for (let i = 0; i < allcategories.length; i++) {
      if (allcategories[i].categorie === "restauration") {
        restauration = allcategories[i].categorie
        restaurationId = allcategories[i]._id
      } else if (allcategories[i].categorie === "Commerce de détail") {
        commerce = allcategories[i].categorie
        commerceId = allcategories[i]._id
      } else if (allcategories[i].categorie === "transports") {
        transports = allcategories[i].categorie
        transportsId = allcategories[i]._id
      } else if (allcategories[i].categorie === "hebergement") {
        hebergement = allcategories[i].categorie
        hebergementId = allcategories[i]._id
      } else {
        autre = allcategories[i].categorie
        autreId = allcategories[i]._id
      }
    };

    var categoriesid = [restaurationId, commerceId, transportsId, hebergementId, autreId]
    var categoriesname = [restauration, commerce, transports, hebergement, autre]
    //console.log("categoriesname",categoriesname)
    var p = [];
    for (var i = 0; i < categoriesid.length; i++) {

      factures[i] = '';
      p[i] = 0;
    }
    //console.log("this.state.list",this.state.list)
    for (var i = 0; i < this.state.list.length; i++) {
      //client_name = this.state.list[i][0]


      if (this.state.list[i]) {
        client_name = this.state.list[i][0]
        //console.log("this.state.list", this.state.list[i])
        if (this.state.list[i][2] >= this.state.datedebut && this.state.list[i][2] <= this.state.datefin) {
          //console.log("this.state.list 2", this.state.list[i])
          this.state.listId[w] = this.state.list[i][3]
          // console.log("this.state.list[i][6]", this.state.list[i][6])
          for (var r = 0; r < categoriesid.length; r++) {
            //console.log("categoriescsv[r]", this.state.list[i][6],categoriesid[r])
            if (this.state.list[i][6] === categoriesname[r]) {
              //console.log("this.state.list[i][6]", this.state.list[i][6])
              categoriescsv[r] = categoriesname[r]
              //console.log("categoriescsv[r]",categoriescsv[r])
              if (this.state.list[i][4] == 0) {

                var n = p[r] + 1

                factures[r] += "\n\nfacture " + n + "\n\n      son montant: " + this.state.list[i][1] + " €" + "\n\n      date de demande: " + this.state.list[i][2]




              } else {

                var n = p[r] + 1

                factures[r] += "\n\nfacture " + n + "\n\n      son montant: " + this.state.list[i][1] + " €" + "\n\n      sa TVA: " + this.state.list[i][4] + " €" + "\n\n      son montant hors taxes: " + this.state.list[i][5] + " €" + "\n\n      date de demande: " + this.state.list[i][2]



              } p[r]++
            }
          }


          w++
        }
      }




      j++
    }
    var nbfac = 0;
    //console.log("categoriescsv", categoriescsv)
    // console.log("factures", this.factures)
    //console.log("p", p)
    for (var i = 0; i < p.length; i++) {
      nbfac += p[i]
    }
    var date_non_précisée = 'précisez une date de début et une date de fin pour générer la note de frais'
    var csvcat = '';
    if (this.state.datedebut == '' || this.state.datefin == '') {
      csv = "\n\n\n\n" + date_non_précisée
      //console.log("csv", csv)
    } else {
      for (var r = 0; r < categoriesid.length; r++) {
        if (categoriescsv[r]) {
          csvcat += "\n\n pour la catégorie : " + categoriescsv[r] + "\n" + factures[r]
          console.log("csv cat 1", csvcat)
        }

      }
      console.log("csv cat", csvcat)
      if (csvcat.length == 0) {
        csv = "\n\n\n\n le(la) client(e) " + client_name + " n'a pas de factures dans la période du:" + this.state.datedebut + " à " + this.state.datefin
      } else {
        csv = "\n\n\n\n le(la) client(e) " + client_name + " a " + nbfac + " facture(s) dans la période du: " + this.state.datedebut + " à " + this.state.datefin + " est/sont la/les suivante(s) :" + csvcat
      }
    }

    this.state.filename = client_name + "_" + Date.now()
    //console.log(csv)
    //console.log("id traites", this.state.listId)
    //console.log("listid length ", this.state.listId.length)
    return csv
  }
  onSubmit(e) {
    //e.preventDefault()



    const factureObject = {
      //name: this.state.name,
      //traitee: this.state.traitee
      traitee: true
    };
    for (var i = 0; i < this.state.listId.length; i++) {
      console.log("listid length ", this.state.listId[i])
      axios.put(`http://localhost:8080/factures/${this.state.listId[i]}`, factureObject)
        .then((res) => {
          console.log(res.data)
          console.log('Facture successfully updated')
        }).catch((error) => {
          console.log(error)
        })
    }


    // Redirect to User List 
    //this.props.history.push('/userfacture/' + this.state.userId)
    return;
  }
  render() {
    //console.log("datatable: ",this.DataTable())
    //console.log("datatable2: ",this.DataTable2())
    //console.log("datatable: ",this.DataTable().length)
    //console.log("datatable_length: ", this.DataTable_length())
    //console.log("datatable2_length: ", this.DataTable2_length())

    for (var i = 0; i < this.state.users.length; i++) {
      if (this.state.users[i]) {
        if (this.state.users[i]._id == this.props.match.params.userId) {
          //console.log("all users", this.state.users[i]._id)
          this.state.user = this.state.users[i].username
          //console.log("user", this.state.user)
        }

      }
    }

    // console.log("le client", this.props.match.params.userId)
    return (
      <>
        <Button
          startIcon={<ArrowBackIcon />}
          size="small"
          style={{
            fontSize: 8,
            marginLeft: '-10%',
            marginTop: '8%'
          }}
          variant="contained"
          color="secondary"
          href={"../user"}>

        </Button>
        <Typography
          variant="h5"
          color='primary'
          style={{ marginTop: '0%', marginLeft: '200px', marginBottom: '0%' }}>
          Espace de gestion des factures
        </Typography>
        <Typography
          variant="h7"
          color='primary'
          style={{ marginTop: '0%', marginLeft: '200px', marginBottom: '0%' }}>
          Le client : {this.state.user}
        </Typography>

        <Paper className="paper-ndf">
          <Typography
            variant='h5'
            color='secondary'
            style={{ marginLeft: '10%' }}
          >
            Télécharger la note de frais :
          </Typography>
          <br /><br />

          <TextField
            variant="filled"
            color="secondary"
            type="date"
            id="date"
            label="Date Début"
            InputLabelProps={{ shrink: true, margin: "dense" }}
            value={this.state.datedebut}
            style={{ marginBottom: '10px', marginLeft: '10%' }}
            onChange={this.onChangeDatedebut}
          />
          <TextField
            variant="filled"
            color="secondary"
            type="date"
            id="date"
            label="Date fin"
            InputLabelProps={{ shrink: true, margin: "dense" }}
            value={this.state.datefin}
            style={{ marginBottom: '10px', marginLeft: '10%' }}
            onChange={this.onChangeDatefin}
          />

          <CSVLink data={this.ExportCSV()} filename={"Note de frais de_" + this.state.filename + ".csv"}  >
            <Button
              startIcon={<SaveIcon />}
              endIcon={<SaveIcon />}
              size="small"
              style={{
                fontSize: 10, marginLeft: '10%'
              }}
              variant="contained"
              color="primary"
              onClick={() => {

                { this.ExportCSV() };
                { this.onSubmit() };
                { window.location.reload() };

              }}>
              note de frais
            </Button>
          </CSVLink>
        </Paper>


        <TableContainer component={Paper} className="table-container2" >
          <Typography
            variant='h5'
            color='primary'

          >
            La liste des factures non traitées :
          </Typography>
          <br />
          <Table size="medium" aria-label="a dense table">
            {this.DataTable_header()}
            <TableBody>
              {this.DataTable()}
            </TableBody>
          </Table>
        </TableContainer>

        <TableContainer component={Paper} className="table-container3" >
          <Typography
            variant='h5'
            color='primary'

          >
            La liste des factures traitées :
          </Typography>
          <br />
          <Table size="medium" aria-label="a dense table">
            {this.DataTable2_header()}
            <TableBody>
              {this.DataTable2()}
            </TableBody>
          </Table>
          <div
            ref={loadingRef => (this.loadingRef = loadingRef)} >
          </div>
        </TableContainer>




      </>
    );


    /*  return (
        
        <div className="row">  
            
           <br/><br/>
           <div>
       <Button variant="danger" href={"../user/"} size="lg" block="block" type="submit">
                    retour
                  </Button>
         </div>
         <br/><br/>
           <div>
           <h2>Espace de l'utilisateur</h2>
           </div>
           
         
        
           
          
       <div>
        <div className="col table-wrapper">
       
          <Table striped bordered hover>
          <thead>
            <tr><b>Factures non Traitées</b></tr>
            <tr>
              <th>Nom de l'utilisateur</th>
              <th>Nom de la Facture</th>
              <th>Montant</th>
              <th>Action</th>
            </tr>
          </thead>
            <tbody>
              {this.DataTable()}
            </tbody>
          </Table>
          
    <div
            ref={loadingRef => (this.loadingRef = loadingRef)} >
          </div>
        </div>
    
        <br/><br/>
        <div className="col table-wrapper">
       
          <Table striped bordered hover>
          <thead>
              <tr> <b>Factures Traitées</b></tr>
              <tr>
              <th>Nom de l'utilisateur</th>
                <th>Nom de la Facture</th>
                <th>Montant</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {this.DataTable2()}
            </tbody>
          </Table>
          <div
            ref={loadingRef => (this.loadingRef = loadingRef)} >
          </div>
        </div>
        </div>
        </div>
      
    );*/
  }


}