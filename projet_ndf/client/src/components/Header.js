import React, { Component } from "react";
import { AppBar, Toolbar, Grid, Button } from '@material-ui/core';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import InfoIcon from '@material-ui/icons/Info';
import AuthService from "../services/auth.service";
import Box from '@material-ui/core/Box';
import LockOpenIcon from '@material-ui/icons/LockOpen';
export default class Header extends Component {
    constructor(props) {
        super(props);
        this.logOut = this.logOut.bind(this);

        this.state = {
            currentUser: undefined,
        };
    }

    componentDidMount() {
        const user = AuthService.getCurrentUser();
        if (user) {
            this.setState({
                currentUser: user

            });
        }
    }

    logOut() {
        AuthService.logout();
    }

    render() {
        const { currentUser } = this.state;
        return (
            <div className="appMain">
                <AppBar  style={{backgroundColor: '#303a5a',  marginLeft: '0%'}}>
                    <Toolbar >
                        <Grid container>
                            <Grid item sm={10}>

                                Gestion des notes de frais
                            </Grid>
                            <Grid item sm={2} >

                                



                                    {currentUser ? (
                                        <>

                                        <Box display="flex" justifyContent="space-between">

                                            <Button
                                                startIcon={<InfoIcon />}
                                                size="md"
                                                style={{
                                                    fontSize: 8
                                                }}
                                                variant="contained"
                                                color="primary"
                                                href={"/home"}

                                            >
                                                {currentUser.username}
                                            </Button>

                                            <Button
                                                startIcon={<ExitToAppIcon />}
                                                size="md"
                                                style={{
                                                    fontSize: 8
                                                }}
                                                variant="contained"
                                                color="primary"
                                                href={"/login"}
                                                onClick={this.logOut}
                                            >
                                                logOut
                                            </Button>
                                        </Box>
                                        </>
                                    ) : (
                                        <>
                                            <Box display="flex" justifyContent="space-between">
                                            <Button
                                                startIcon={<LockOpenIcon />}
                                                size="md"
                                                style={{
                                                    fontSize: 8
                                                }}
                                                variant="contained"
                                                color="primary"
                                                href={"/login"}
                                                
                                            >
                                                logIn
                                            </Button>

                                            
                                            <Button
                                                startIcon={<LockOpenIcon />}
                                                size="md"
                                                style={{
                                                    fontSize: 8
                                                }}
                                                variant="contained"
                                                color="primary"
                                                href={"/register"}
                                                
                                            >
                                                Sign Up
                                            </Button>
                                            </Box>
                                        </>
                                    )}

                                
                            </Grid>
                        </Grid>

                    </Toolbar>
                </AppBar>
            </div>
        )
    }



}