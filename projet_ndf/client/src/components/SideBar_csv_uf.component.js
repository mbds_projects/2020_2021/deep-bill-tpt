import React, { Component } from "react";
import axios from 'axios';
import SaveIcon from '@material-ui/icons/Save';
import { Grid, Button } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { Card } from 'react-bootstrap';
import "../App.css";
import AuthService from "../services/auth.service";
let restaurationId, commerceId, transportsId, hebergementId, restauration, commerce, transports, hebergement, autre, autreId
var allcategories
export default class SideBar_csv_uf extends Component {
  constructor(props) {
    super(props)

    this.onChangeDatedebut = this.onChangeDatedebut.bind(this);
    this.onChangeDatefin = this.onChangeDatefin.bind(this);

    this.state = {
      factures: [],
      list: [],
      filename: '',
      datedebut: '',
      datefin: '',
      listId: [],
      FactureCategorie: [],
      currentUser: undefined,
    };
  }

  onChangeDatedebut(e) {
    this.setState({
      datedebut: e.target.value
    });
    //console.log("date debut", this.state.datedebut)
  }

  onChangeDatefin(e) {
    this.setState({
      datefin: e.target.value
    });
  }

  getFactures() {


    axios.get(`http://localhost:8080/factures/userfacture`)
      .then(res => {
        var k = 0
        //console.log("je suis le res2",res.data)
        //console.log("je suis le res2 length",res.data.length)
        // console.log("current user",this.state.currentUser)
        //console.log(" res.data[i]",res)
       /* for (var i = 0; i < res.data.length; i++) {
          if (this.state.currentUser) {
            if (this.state.currentUser.id == res.data[i].userId) {
              this.state.factures[k] = res.data[i]
              k++
            }

          }

        }*/
        this.setState({ factures: [...this.state.factures, ...res.data] });
        
      });


  };

  getCategories(page) {
    axios.get('http://localhost:8080/users/test/allCategorie')
      .then(resp => {

        this.setState({ FactureCategorie: [...resp.data] });
        console.log("all categorie", this.state.FactureCategorie)
      });

  }



  componentDidMount() {
    console.log("componentDidMount")
    const user = AuthService.getCurrentUser();
    if (user) {
      this.setState({
        currentUser: user
      });
    }

    this.getFactures(this.state.page);
    this.getCategories();
  }

  padLeadingZeros(num, size) {
    var s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
  }
  ExportCSV() {
    var table = []
    console.log("je suis this.props", this.props.obj)
    //console.log("je suis user", this.state.currentUser)
    //console.log("les factures :", this.state.factures)
    this.state.factures.map((res, i) => {
      if (res.traitee === false) {

        var nowDate = new Date(this.state.factures[i].createdAt);
        var year = nowDate.getFullYear();
        year = year.toString()
        var month = nowDate.getMonth() + 1;
        if (month < 10) {
          month = this.padLeadingZeros(month, 2);
        } else {
          month = month.toString();
        }

        //console.log("le mois :", month1)
        //console.log("type de mois :", typeof(month1))


        var day = nowDate.getDate();
        day = day.toString()
        //console.log("le jour :", day1)
        //console.log("type de jour :", typeof(day1))
        //var date = day + '/' + (month + 1) + '/' + year;
        var date = year + '-' + month + '-' + day;
        var n, m, id, mht, tva, c
        n = this.state.factures[i].username
        m = this.state.factures[i].montant
        mht = this.state.factures[i].montant_ht
        tva = this.state.factures[i].tva
        id = this.state.factures[i]._id
        c = this.state.factures[i].categories[0]
        this.state.list[i] = [n, m, date, id, tva, mht, c]
        // console.log("list csv :", this.state.list)



      }
      //console.log(this.state.list.size)
    });
    var csv = ''
    var client_name = ''

    //console.log("rrr", this.state.list)
    //console.log("length", this.state.list.length)
    //console.log("client name:", client_name)
    var facture = ''
    var factures = [];
    var categoriescsv = [];
    var j = 0, w = 0
    allcategories = this.state.FactureCategorie.map(r => {
      return {
        categorie: r.catégorie,
        _id: r._id
      }
    })
    //console.log("all categories", allcategories)
    for (let i = 0; i < allcategories.length; i++) {
      if (allcategories[i].categorie === "restauration") {
        restauration = allcategories[i].categorie
        restaurationId = allcategories[i]._id
      } else if (allcategories[i].categorie === "Commerce de détail") {
        commerce = allcategories[i].categorie
        commerceId = allcategories[i]._id
      } else if (allcategories[i].categorie === "transports") {
        transports = allcategories[i].categorie
        transportsId = allcategories[i]._id
      } else if (allcategories[i].categorie === "hebergement") {
        hebergement = allcategories[i].categorie
        hebergementId = allcategories[i]._id
      } else {
        autre = allcategories[i].categorie
        autreId = allcategories[i]._id
      }
    };

    var categoriesid = [restaurationId, commerceId, transportsId, hebergementId, autreId]
    var categoriesname = [restauration, commerce, transports, hebergement, autre]
    //console.log("liste categories id", categoriesid)
    //console.log("liste categories name", categoriesname)
    var p = [];
    for (var i = 0; i < categoriesid.length; i++) {
      factures[i] = '';
      p[i] = 0;
    }
    for (var i = 0; i < this.state.list.length; i++) {
      //client_name = this.state.list[i][0]


      if (this.state.list[i]) {
        //console.log("je suis la date textfield: ", this.state.datedebut)
        //console.log("je suis la date db: ", this.state.list[i][2])
        client_name = this.state.list[i][0]
        if (this.state.list[i][2] >= this.state.datedebut && this.state.list[i][2] <= this.state.datefin) {

          this.state.listId[w] = this.state.list[i][3]
          for (var r = 0; r < categoriesid.length; r++) {

            if (this.state.list[i][6] == categoriesid[r]) {
              //console.log("je suis la categorie du id:", categoriesname[r])
              categoriescsv[r] = categoriesname[r]

              if (this.state.list[i][4] == 0) {

                var n = p[r] + 1

                factures[r] += "\n\nfacture " + n + "\n\n      son montant: " + this.state.list[i][1] + " €" + "\n\n      date de demande: " + this.state.list[i][2]




              } else {

                var n = p[r] + 1

                factures[r] += "\n\nfacture " + n + "\n\n      son montant: " + this.state.list[i][1] + " €" + "\n\n      sa TVA: " + this.state.list[i][4] + " €" + "\n\n      son montant hors taxes: " + this.state.list[i][5] + " €" + "\n\n      date de demande: " + this.state.list[i][2]



              } p[r]++

            }
          }


          w++
        }
      }



      //console.log(facture)
      j++
    }
    //var facturescsv = '';
    //var temp = [];
    /*  for (var r = 0; r < categoriesid.length; r++){
        
        if(factures[r]){
          var temp = factures[r].split("\n");
          //console.log("temp", temp);
        }
        
        
        
      }*/
    var nbfac = 0;
    console.log("p", p)
    for (var i = 0; i < p.length; i++) {
      nbfac += p[i]
    }
    var date_non_précisée = 'précisez une date de début et une date de fin pour générer la note de frais'
    var csvcat = '';
    if (this.state.datedebut == '' || this.state.datefin == '') {
      csv = "\n\n\n\n" + date_non_précisée
    } else {
      for (var r = 0; r < categoriesid.length; r++) {
        if (categoriescsv[r]) {
          csvcat += "\n\n pour la catégorie : " + categoriescsv[r] + "\n" + factures[r]
        }

      }
      console.log(" csvcat: \n", csvcat.length);
      if (csvcat.length == 0) {
        csv = "\n\n\n\n le(la) client(e) " + client_name + " n'a pas de factures dans la période du:" + this.state.datedebut + " à " + this.state.datefin
      } else {
        csv = "\n\n\n\n le(la) client(e) " + client_name + " a " + nbfac + " facture(s) dans la période du: " + this.state.datedebut + " à " + this.state.datefin + " est/sont la/les suivante(s) :" + csvcat
      }

    }
    this.state.filename = client_name + "_" + Date.now()
    console.log(csv)

    //var temp = csv.split("\n");
    //console.log("temp csv: \n", temp, temp.length);
    /* var csvfinal = '';
   
     for (var i = 0; i < temp.length; i++){
        if(temp[i] != "undefined"){
         csvfinal += temp[i]
         console.log("csvfinal: \n", csvfinal);
        }
   
     }*/


    return csv
  }


  onSubmit(e) {
    //e.preventDefault()



    const factureObject = {
      //name: this.state.name,
      //traitee: this.state.traitee
      traitee: true
    };
    for (var i = 0; i < this.state.listId.length; i++) {
      console.log("listid length ", this.state.listId[i])
      axios.put(`http://localhost:8080/factures/${this.state.listId[i]}`, factureObject)
        .then((res) => {
          console.log(res.data)
          console.log('Facture successfully updated')
        }).catch((error) => {
          console.log(error)
        })
    }


    // Redirect to User List 
    //this.props.history.push('/userfacture/' + this.state.userId)
    return;
  }
  render() {
    return (
      <div className="side-bar-csv">
        <Grid container>

          <Grid item sm={10} style={{ marginTop: '30%' }}>
            <Card.Body>
              <Typography
                variant='40'

              >
                Télécharger la note de frais :
              </Typography>
              <br /><br />

              <TextField
                variant="filled"
                color="secondary"
                type="date"
                id="date"
                label="Date Début"
                InputLabelProps={{ shrink: true, margin: "dense" }}
                value={this.state.datedebut}
                style={{ marginBottom: '10px' }}
                onChange={this.onChangeDatedebut}
              />



              <TextField
                variant="filled"
                color="secondary"
                type="date"
                id="date"
                label="Date fin"
                InputLabelProps={{ shrink: true, margin: "dense" }}
                value={this.state.datefin}
                style={{ marginBottom: '10px' }}
                onChange={this.onChangeDatefin}
              />



              

                <Button
                  startIcon={<SaveIcon />}
                  endIcon={<SaveIcon />}
                  size="small"
                  style={{
                    fontSize: 10
                  }}
                  variant="contained"
                  color="primary"
                  onClick={() => {

                    { this.ExportCSV() };
                   

                  }}>
                  note de frais
                </Button>
              


            </Card.Body>

          </Grid>
        </Grid>


      </div>
    )
  }



}