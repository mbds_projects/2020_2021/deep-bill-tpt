import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import AuthService from "../services/auth.service";
import CreateUserFacture from "./create-userFacture.component";
import { Card} from 'react-bootstrap';
import {  Button } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Typography from '@material-ui/core/Typography';
export default class UploadUserFacture extends Component {
  constructor(props) {
    super(props);

    this.state = {
      redirect: null,
      userReady: false,
      currentUser: { username: "" }
    };
  }
 
  componentDidMount() {
    const currentUser = AuthService.getCurrentUser();
    if (!currentUser) this.setState({ redirect: "/home" });
    this.setState({ currentUser: currentUser, userReady: true })
    
    
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />
    }

    return (
      <div>
         <Button
                        startIcon={<ArrowBackIcon />}
                        size="small"
                        style={{
                            fontSize: 8,
                            marginLeft: '-10%',
                            marginTop: '8%'
                        }}
                        variant="contained"
                        color="secondary"
                        href={"../assistant"}>

                    </Button>
      
        
      <Card style={{ width: '50rem' , marginTop: '5%'}}>
         
      
        {(this.state.userReady) ?
        <div>
          <Typography
          variant="h4"
          color='primary'
          style={{ marginTop: '40px', marginLeft: '150px',marginBottom: '8%' }}>
          Envoyer les factures d'un client
        </Typography>
        <CreateUserFacture/>
      </div>: null}
      
      </Card>
      </div>
    );
  }
}
