import React, { Component } from 'react';
import { Button } from '@material-ui/core';
import DetailsIcon from '@material-ui/icons/Details';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
export default class Facture extends Component {

  render() {
    return (



      <TableRow >
        {/* <Card.Img src={`http://localhost:8080/${this.props.obj.image}`} variant='top' /> */}
        <TableCell component="th" scope="row">
        {this.props.obj.name}
        </TableCell>
        <TableCell align="center" component="th" scope="row">
        
                < Button 
                startIcon={<DetailsIcon />}
                size="small"
                style={{
                  fontSize: 10
                }}
                variant="contained"
                href={"/detailpersonnelassistant/" + this.props.obj._id}
                color="primary">
                  Detail
                  </Button>
              
        </TableCell>
       
        </TableRow>

    );
  }
}

