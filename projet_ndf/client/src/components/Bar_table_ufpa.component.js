import React, { Component } from "react";
import axios from 'axios';
import Typography from '@material-ui/core/Typography';
import "../App.css";
import AuthService from "../services/auth.service";
import UserFacturePersonnelleTableAssistant from './UserFacturePersonnelleTableAssistant';
import UserFacturePersonnelleTableAssistant2 from './UserFacturePersonnelleTableAssistant2';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

export default class Bar_table_ufpa extends Component {
  constructor(props) {
    super(props)



    this.state = {
      factures: [],
      list: [],
      filename: '',
      datedebut: '',
      datefin: '',
      listId: [],
      FactureCategorie: [],
      currentUser: undefined,
    };
  }



  getFactures() {


    axios.get(`http://localhost:8080/factures/factures`)
      .then(res => {
        var k = 0
        //console.log("je suis le res2",res.data)
        //console.log("je suis le res2 length",res.data.length)
        // console.log("current user",this.state.currentUser)
        for (var i = 0; i < res.data.length; i++) {
          if (this.state.currentUser) {
            if (this.state.currentUser.id == res.data[i].userId) {
              this.state.factures[k] = res.data[i]
              k++
            }

          }

        }
        //this.setState({ factures: [...this.state.factures, ...res.data] });
        console.log("je suis this.state.factures", this.state.factures)
      });


  };

  getCategories(page) {
    axios.get('http://localhost:8080/users/test/allCategorie')
      .then(resp => {

        this.setState({ FactureCategorie: [...resp.data] });
        console.log("all categorie", this.state.FactureCategorie)
      });

  }



  componentDidMount() {
    console.log("componentDidMount")
    const user = AuthService.getCurrentUser();
    if (user) {
      this.setState({
        currentUser: user
      });
    }

    this.getFactures(this.state.page);
    this.getCategories();
  }

  DataTable() {
    return this.state.factures.map((res, i) => {
      if (res.traitee === false) {
        return <UserFacturePersonnelleTableAssistant obj={res} key={i} />
      }
    });
  }
  DataTable_length() {
    var table = []
    var k = 0

    this.state.factures.map((res, i) => {
      if (res.traitee === false) {
        table[i] = <UserFacturePersonnelleTableAssistant obj={res} key={i} />
        k++
      }
    });
    return k
  }

  DataTable_header() {
    //var header = <thead><h3>Le client n'a aucune Facture non traitée</h3></thead>
    var header = <Typography
      variant="h4"
      color='secondary'>
      Vous n'avez aucune facture à traiter
    </Typography>
    var header2 = <TableHead >
      <TableRow>
        <TableCell align="center">Nom de la Facture</TableCell>
        <TableCell align="center">Date de la demande</TableCell>
        <TableCell align="center">Action</TableCell>
      </TableRow>
    </TableHead>


    if (this.DataTable_length() === 0) {
      return header
    } else {
      return header2
    }

  }

  DataTable2() {
    var table2 = []

    this.state.factures.map((res, i) => {

      if (res.traitee === true) {

        table2[i] = <UserFacturePersonnelleTableAssistant2 obj={res} key={i} />

      }
    });
    return table2
  }
  DataTable2_length() {
    var table2 = []
    var k = 0

    this.state.factures.map((res, i) => {
      if (res.traitee === true) {

        table2[i] = <UserFacturePersonnelleTableAssistant2 obj={res} key={i} />
        k++
      }
    });

    return k
  }
  DataTable2_header() {
    //var header = <thead><h3>Le client n'a aucune facture traitée</h3></thead>
    var header = <Typography
      variant="h4"
      color='secondary'>
      Vous n'avez aucune facture traitée
    </Typography>
    var header2 = <TableHead >
      <TableRow>
        <TableCell >Nom de la Facture</TableCell>
        <TableCell align="center">Action</TableCell>
      </TableRow>
    </TableHead>



    if (this.DataTable2_length() === 0) {
      return header;
    } else {
      return header2
    }

  }


  render() {
    const { currentUser } = this.state;

    return (
      <>
      <div className="bar-table">
        {currentUser && (
          <Typography
            variant="h5"
            color='primary'
            style={{ marginTop: '10%', marginLeft: '10%' }}

          >
            Bonjour {currentUser.username}

          </Typography>)}
        <Typography
          variant="h7"
          color='primary'
          style={{ marginTop: '0%', marginLeft: '10%' }}>
          Dans cet espace vous pouvez gérer vos factures personnelles
        </Typography>

        <TableContainer component={Paper} className="table-container" >
          <Typography
            variant="h5"
            color='primary'
          >
            La liste des factures non traitées :
          </Typography>

          <Table size="medium" aria-label="a dense table">
            {this.DataTable_header()}
            <TableBody>
              {this.DataTable()}
            </TableBody>
          </Table>
        </TableContainer>

        <TableContainer component={Paper} className="table-container" >
          <Typography
            variant="h5"
            color='primary'

          >
            La liste des factures traitées :
          </Typography>

          <Table size="medium" aria-label="a dense table">
            {this.DataTable2_header()}
            <TableBody>
              {this.DataTable2()}
            </TableBody>
          </Table>
          
        </TableContainer>


      </div>
      <div
      ref={loadingRef => (this.loadingRef = loadingRef)} >
    </div>
     </>
    )
  }



}