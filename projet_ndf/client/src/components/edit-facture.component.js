import React, { Component } from "react";
import { Card, ListGroup, Form } from 'react-bootstrap';
import axios from 'axios';
import "../App.css";
import { Button } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import UpdateIcon from '@material-ui/icons/Update';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';
let restaurationId, commerceId, transportsId, hebergementId, restauration, commerce, transports, hebergement, autre, autreId

export default class EditFacture extends Component {

  constructor(props) {
    super(props)


    this.onChangeFactureCategorie = this.onChangeFactureCategorie.bind(this);
    this.onChangeCategorieName = this.onChangeCategorieName.bind(this);
    this.onChangeFactureName = this.onChangeFactureName.bind(this);
    this.onChangeFactureMontant = this.onChangeFactureMontant.bind(this);
    this.onChangeFactureTVA = this.onChangeFactureTVA.bind(this);
    this.onChangeFactureMontantHT = this.onChangeFactureMontantHT.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    // State
    this.state = {
      name: '',
      categories: '',
      montant: '',
      tva: '',
      montant_ht: '',
      cat: '',
      image: "",
      FactureCategorie: []
    }
  }
  getCategories() {
    axios.get('http://localhost:8080/users/test/allCategorie')
      .then(resp => {
        //console.log("test/allcategorie", resp.data)
        this.setState({ FactureCategorie: [...this.state.FactureCategorie, ...resp.data] });

      });

    console.log("this.state.FactureCategorie1", this.state.FactureCategorie.length)
  }

  getfacture() {
    axios.get('http://localhost:8080/factures/edit/' + this.props.match.params.id)
      .then(res => {
        //console.log("facture data", res)
        this.setState({
          name: res.data.name,
          cat: res.data.categories,
          categories: res.data.categories,
          montant: res.data.montant,
          tva: res.data.tva,
          montant_ht: res.data.montant_ht,
          image: res.data.image,
          userId: res.data.userId
        });

      })
      .catch((error) => {
        console.log(error);
      })
  }



  componentDidMount() {
    this.getCategories();
    this.getfacture();

  }
  onChangeFactureCategorie(e) {
    this.setState({ categories: e.target.value })
  }

  onChangeCategorieName(e) {
    this.setState({ categories: e.target.value })
    /*
        for (let i = 0; i < allcategories.length; i++) {
          if (allcategories[i].categorie === "restauration") {
            restauration = allcategories[i].categorie
            restaurationId = allcategories[i]._id
          } else if (allcategories[i].categorie === "Commerce de détail") {
            commerce = allcategories[i].categorie
            commerceId = allcategories[i]._id
          } else if (allcategories[i].categorie === "transports") {
            transports = allcategories[i].categorie
            transportsId = allcategories[i]._id
          } else if (allcategories[i].categorie === "hebergement") {
            hebergement = allcategories[i].categorie
            hebergementId = allcategories[i]._id
          } else {
            autre = allcategories[i].categorie
            autreId = allcategories[i]._id
          }
        };
    
        switch (e.target.value) {
          case "restauration":
            this.setState({ categories: restauration })
            break
          case "Commerce de détail":
            this.setState({ categories: commerce })
            break
          case "transports":
            this.setState({ categories: transports })
            break
          case "hebergement":
            this.setState({ categories: hebergement })
            break
          case "autre":
            this.setState({ categories: autre })
            break
        }
        */
  }

  onChangeFactureName(e) {
    this.setState({ name: e.target.value })
  }

  onChangeFactureMontant(e) {
    this.setState({ montant: e.target.value })
  }

  onChangeFactureTVA(e) {
    this.setState({ tva: e.target.value })
  }

  onChangeFactureMontantHT(e) {
    this.setState({ montant_ht: e.target.value })
  }


  onSubmit(e) {
    e.preventDefault()

    const factureObject = {
      name: this.state.name,
      categories: this.state.categories,
      montant: this.state.montant,
      tva: this.state.tva,
      montant_ht: this.state.montant_ht
    };
    axios.put('http://localhost:8080/factures/' + this.props.match.params.id, factureObject)
      .then((res) => {
        console.log(res.data)
        console.log('Facture successfully updated')
      }).catch((error) => {
        console.log(error)
      })

    // Redirect to User List 
    this.props.history.push('/userfacture/' + this.state.userId)
  }


  render() {

    return (
      <>
        <Button
          startIcon={<ArrowBackIcon />}
          size="small"
          style={{
            fontSize: 8,
            marginTop: '8%',
            marginLeft: '-10%'
          }}
          variant="contained"
          color="secondary"
          href={"/userfacture/" + this.state.userId}>

        </Button>
        <Typography
          variant="h6"
          color='primary'
          style={{ marginTop: '-30px', marginLeft: '250px' }}>
          Les informations de la facture sont saisies automatiquement
        </Typography>
        <Typography
          variant="h7"
          color='primary'
          style={{ marginTop: '-5px', marginLeft: '250px' }}>
          En cas d'erreur, vous pouvez les corriger via ce formulaire
        </Typography>

        <Paper className="paper-efa">
          <Card.Img style={{ width: '25rem', height: '35rem' }} src={`http://localhost:8080/${this.state.image}`} />

        </Paper>

        <Paper className="paper-efa2">
          <ListGroup className="list-group-flush" style={{ marginLeft: '20%' }}>
            <Form onSubmit={this.onSubmit}>

              <Form.Group controlId="categoriename" >
                <Form.Label><b> categorie : </b>{this.state.categories}</Form.Label>
              </Form.Group>
              <div class="form-group">

                <FormControl variant="filled" onChange={this.onChangeCategorieName} multiple style={{ marginBottom: '2%', marginLeft: '5%', width: '61%' }}>
                  <InputLabel htmlFor="filled-age-native-simple" >Catégorie</InputLabel>
                  <Select
                    native

                  >
                    <option aria-label="None" value="" />
                    {this.state.FactureCategorie.map(r => (
                      <option
                        key={r._id}
                        value={r.catégorie}
                      >
                        {r.catégorie}
                      </option>))}
                  </Select>
                </FormControl>
              </div>

              <Form.Group controlId="Name">
                <Form.Label>Montant</Form.Label>

                <TextField
                  variant="filled"
                  color="secondary"
                  type="text"
                  label="Mntant Brut"
                  InputLabelProps={{ shrink: true, margin: "dense" }}
                  value={this.state.montant}
                  style={{ marginBottom: '2%', marginLeft: '5%' }}
                  onChange={this.onChangeFactureMontant}
                />
              </Form.Group>

              <Form.Group controlId="Name">
                <Form.Label>TVA</Form.Label>

                <TextField
                  variant="filled"
                  color="secondary"
                  type="text"
                  label="TVA"
                  InputLabelProps={{ shrink: true, margin: "dense" }}
                  value={this.state.tva}
                  style={{ marginBottom: '2%', marginLeft: '5%' }}
                  onChange={this.onChangeFactureTVA}
                />
              </Form.Group>

              <Form.Group controlId="Name">
                <Form.Label>Montant hors taxes</Form.Label>

                <TextField
                  variant="filled"
                  color="secondary"
                  type="text"
                  label="Mntant HT"
                  InputLabelProps={{ shrink: true, margin: "dense" }}
                  value={this.state.montant_ht}
                  style={{ marginLeft: '5%' }}
                  onChange={this.onChangeFactureMontantHT}
                />

              </Form.Group>

              <Button
                startIcon={<UpdateIcon />}
                size="md"
                style={{
                  fontSize: 10,
                  marginTop: 20,
                  marginLeft: 50
                }}
                variant="contained"
                type="submit"
                color="secondary"
              >
                Update Facture
              </Button>


            </Form>

          </ListGroup>

        </Paper>

      </>);
  }
}
