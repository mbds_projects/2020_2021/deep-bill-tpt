import React, { Component } from "react";
import { Card } from 'react-bootstrap';
import { Button } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import axios from 'axios';
import Paper from '@material-ui/core/Paper';
import DeleteIcon from '@material-ui/icons/Delete';

export default class DeleteFacture extends Component {

  constructor(props) {
    super(props)


    this.onSubmit = this.onSubmit.bind(this);

    // State
    this.state = {
      name: '',
      montant: '',
      image: ""
    }
  }

  componentDidMount() {
    axios.get('http://localhost:8080/factures/edit/' + this.props.match.params.id)
      .then(res => {
        this.setState({
          name: res.data.name,
          montant: res.data.montant,
          image: res.data.image,
          userId: res.data.userId
        });
      })
      .catch((error) => {
        console.log(error);
      })
  }




  onSubmit(e) {
    e.preventDefault()

    if (window.confirm('Voulez-vous supprimer ce fichier?!')) {
      axios.delete('http://localhost:8080/factures/' + this.props.match.params.id)
        .then(res => {
          //console.log(res);
          //console.log(res.data);
        })
      // Redirect to User List 
      this.props.history.push('/userfacture/' + this.state.userId)
    }
  }

  render() {
    return (
      <>
        <Button
          startIcon={<ArrowBackIcon />}
          size="small"
          style={{
            fontSize: 8,
            marginTop: '8%',
            marginLeft: '-10%'
          }}
          variant="contained"
          color="secondary"
          href={"/userfacture/" + this.state.userId}>

        </Button>

        <Button
          startIcon={<DeleteIcon />}
          size="md"
          style={{
            fontSize: 8,
            marginTop: '8%',
            marginLeft: '10%'
          }}
          variant="contained"
          color="secondary"
          type="submit"
          onClick={this.onSubmit}
        >
          Delete
        </Button>



        <Paper className="paper-dfpa">
          <Card.Img variant="top" src={`http://localhost:8080/${this.state.image}`} />
        </Paper>


      </>);
  }
}
