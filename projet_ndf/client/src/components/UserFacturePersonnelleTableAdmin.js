import React, { Component } from 'react';
import DetailsIcon from '@material-ui/icons/Details';
import DeleteIcon from '@material-ui/icons/Delete';
import { Button } from '@material-ui/core';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Box from '@material-ui/core/Box';
export default class Facture extends Component {

  render() {
    return (



      <TableRow>
        {/* <Card.Img src={`http://localhost:8080/${this.props.obj.image}`} variant='top' /> */}
        <TableCell component="th" scope="row">
        {this.props.obj.name}
        </TableCell>
  
        <TableCell width="35%" component="th" scope="row">
        <Box display="flex" justifyContent="space-between">



            < Button
              startIcon={<DetailsIcon />}
              size="small"
              style={{
                fontSize: 10
              }}
              variant="contained"
              color="primary"
              href={"/detailpersonneladmin/" + this.props.obj._id}
            >
              Detail
            </Button>



            <Button
              startIcon={<DeleteIcon />}
              size="small"
              style={{
                fontSize: 10
              }}
              variant="contained"
              color="primary"
              href={"/deleteadmin/" + this.props.obj._id}
            >
              Delete
            </Button>

          </Box>
        </TableCell>
        </TableRow>

    );
  }
}

