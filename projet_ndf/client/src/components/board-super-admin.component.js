import React, { Component } from "react";
import axios from 'axios';
import UserSuperAdminTableRow from './UserSuperAdminTableRow';
import authHeader from '../services/auth-header';
import AuthService from "../services/auth.service";
import { Button } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Typography from '@material-ui/core/Typography';

export default class BoardSuperAdmin extends Component {
  constructor(props) {
    super(props);

    this.state = {
        content: "",
        users: []
    };
}

componentDidMount() {
    const currentUser = AuthService.getCurrentUser();
    console.log(currentUser.id)

    axios.get('http://localhost:8080/users/test/allUsers', { headers: authHeader() })
        .then(
            response => {
                this.setState({ users: [...this.state.users, ...response.data] });

            },
            error => {
                this.setState({
                    content:
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString()
                });
            }
        );
}
DataTable() {
    const currentUser = AuthService.getCurrentUser();
    return this.state.users.map((res, i) => {
        //console.log("res:",res._id,currentUser.id)
        if(res._id === currentUser.id){
            return;
        }else {
            return <UserSuperAdminTableRow obj={res} key={i} />;
        }
        
    });
}

render() {
    return (
        <>
        <Button
          startIcon={<ArrowBackIcon />}
          size="small"
          style={{
            fontSize: 8,
            marginLeft: '-10%',
            marginTop: '8%'
          }}
          variant="contained"
          color="secondary"
          href={"../home"}>

        </Button>
        <Typography
            variant="h5"
            color='primary'
            style={{ marginTop: '0%', marginLeft: '30%' }}
          >
            Espace de gestion des utilisateurs :
          </Typography>
        
        <TableContainer component={Paper} className="table-container-home" >

          <Table size="medium" aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="left">Nom de l'utilisateur :</TableCell>
                <TableCell align="center">Action :</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
            {this.DataTable()}
            </TableBody>
          </Table>
        </TableContainer>
        </>);
}
}
