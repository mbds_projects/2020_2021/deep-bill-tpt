import React, { Component } from "react";
import axios from 'axios';
import Table from 'react-bootstrap/Table';
import UserFacturenonTraiteeTable from './UserFacturenonTraiteeTable';
import { Button} from 'react-bootstrap';
import { CSVLink } from "react-csv";

export default class UsersFacturesnontraitees extends Component {

    constructor(props) {
        super(props)
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            factures: [],
            traitee : ''
            
        };
       
      }

  getFactures() {
    axios.get(`http://localhost:8080/factures/userfacturenontraitee/${this.props.match.params.userId}`)
      .then(res => {
        /*  for(var i=0; i<res.data.length; i++){
            console.log(res.data[i])
          }*/
          
        //console.log("data to csv: ", res.data);
            
            this.setState({ factures: [...res.data] });
          
         
      });
      
     
  }

  
  
 async componentDidMount() {
    console.log("componentDidMount");
    this.getFactures(this.state.page);
    
  }


  DataTable() {
   
    return this.state.factures.map((res, i) => {
     
      //console.log("holiila",this.state.factures[i].traitee);
      
      return <UserFacturenonTraiteeTable obj={res} key={i} />
      
    });
  }

  ExportCSV() {
    //e.preventDefault()
  var k=0,j=0
  var v,v1,v2,v3
  var list=[]
 let vlist= this.state.factures.map((res, i) => {
       var nowDate = new Date(this.state.factures[i].createdAt );
       var year = nowDate.getFullYear();
       var month = nowDate.getMonth();
       var day = nowDate.getDate();
       var date = day+'/'+(month+1)+'/'+year; 
       v = ["Le client : "+this.state.factures[i].username, " a une Facture avec un montant de : "+this.state.factures[i].montant+" (€)", " il a fait cette demande le : "+date]
     
       k++
       
       return <li key={i}>{v}</li>
    });
    for(var i=0;i<k;i++){
      
      
      list[j]=vlist[i].props.children
      console.log("woooow",vlist[i].props.children,typeof(list[j]))
      j++
    }
  
    console.log("CSV List: ",list,Object.keys(list),typeof(list))
    return list
  }

  toUpdate() {
    //e.preventDefault()
  var k=0,j=0,p=0
  var v,v1
  var list= []
 let vlist= this.state.factures.map((res, i) => {
       v = [this.state.factures[i]._id,this.state.factures[i].username, this.state.factures[i].montant, this.state.factures[i].traitee, this.state.factures[i].createdAt]
      //v1 = [this.state.factures[i]._id] 
      k++
       return <li key={i}>{v}</li>
    });
    for(var i=0;i<k;i++){
      //console.log("woooow",typeof(vlist[i].props.children[0]))
      list[j]=vlist[i].props.children
      j++
    }
    
    
    return list
  }

  onSubmit(e) {
    e.preventDefault()
    var list = []
    list =this.toUpdate()
    if(this.toUpdate().length >0){
      console.log("youupi",this.toUpdate().length)
      for(var i=0; i<this.toUpdate().length; i++){
        this.state.traitee = true
        
        axios.put('http://localhost:8080/factures/' + this.props.match.params.id, this.state.traitee )
        .then((res) => {
          console.log(res.data)
          console.log('Facture successfully updated')
        }).catch((error) => {
          console.log(error)
        })

      }
    }
  
  }


  render() {
    //console.log("vvvvvvvvvv",this.state.factures)
    //console.log("youupi",this.ExportCSV().length)
    //console.log("youupi",this.toUpdate(),Object.keys(this.toUpdate()))
    return (
 
        <div className="row">  
            
        <div className="col table-wrapper">
        <b>La liste des Factures à Traiter</b>
          
         <div ref={loadingRef => (this.loadingRef = loadingRef)} >
         </div>   
        </div>
        <div className="col-md-4 center" >
       
        <Button variant="danger" size="lg" block="block" type="submit">
            <CSVLink  data={this.ExportCSV()} onSubmit={this.onSubmit} >Export</CSVLink>
        </Button>
        
        </div>
        
        <Table striped bordered hover>
             <tr>
                <th>Nom de l'utilisateur</th>
                <th>Nom du Fichier</th>
                <th> Montant de la Facture</th>
                <th> Data to CSV</th>
              </tr>
            <tbody>
            
            {this.DataTable()}
            </tbody>
          </Table>

         
        </div>

  );
  
  }

  
}