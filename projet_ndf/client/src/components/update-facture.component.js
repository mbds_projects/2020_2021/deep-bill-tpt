import React, { Component } from "react";
import { Card} from 'react-bootstrap';
import axios from 'axios';
import { CSVLink } from "react-csv";
import { Button } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Paper from '@material-ui/core/Paper';
import GetAppIcon from '@material-ui/icons/GetApp';
export default class UpdateFacture extends Component {

  constructor(props) {
    super(props)

    //this.onChangeFactureName = this.onChangeFactureName.bind(this);
    //this.onChangeFactureTraitee = this.onChangeFactureTraitee.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    // State
    this.state = {

      name: '',
      montant: '',
      traitee: '',
      image: "",
      username: '',
      userId: '',
      createdAt: ''
    }
  }

  getFacture() {
    axios.get('http://localhost:8080/factures/update/' + this.props.match.params.id)
      .then(res => {
        this.setState({

          name: res.data.name,
          montant: res.data.montant,
          traitee: res.data.traitee,
          image: res.data.image,
          userId: res.data.userId,
          username: res.data.username,
          createdAt: res.data.createdAt
        });
      })
      .catch((error) => {
        console.log(error);
      })

  }
  async componentDidMount() {
    console.log("componentDidMount");
    this.getFacture();

  }
  /* onChangeFactureName(e) {
     this.setState({ name: e.target.value })
   }
 
   onChangeFactureTraitee(e) {
     this.setState({ traitee: e.target.value })
   }
   */
  ExportCSV() {
    //e.preventDefault()


    var n, m
    n = this.state.username
    m = this.state.montant

    var nowDate = new Date(this.state.createdAt);
    var year = nowDate.getFullYear();
    var month = nowDate.getMonth();
    var day = nowDate.getDate();
    var date = day + '/' + (month + 1) + '/' + year;

    var list = "\n\n\nle/la client(e): " + n + " \n\n a une facture avec un montant de: " + m + " €" + "\n\n il/elle a fait la demande le : " + date
 /*var list = "----------------------------------------------------------------------------------------------------------------\n"+
           "    |le/la client(e):          |  le montant                        |      la date                               \n"+
           "-----------------------------------------------------------------------------------------------------------------\n"+
           "   "+n  +"                     |"+"       "+m  +"                   |"+"       "+date  +"                        \n"+
           "-------------------------------------------------------------------------------------------------------------------"  */


    console.log("CSV List: ", list, typeof (list))
    return list
  }

  onSubmit(e) {
    //e.preventDefault()



    const factureObject = {
      //name: this.state.name,
      //traitee: this.state.traitee
      traitee: true
    };

    axios.put('http://localhost:8080/factures/' + this.props.match.params.id, factureObject)
      .then((res) => {
        console.log(res.data)
        console.log('Facture successfully updated')
      }).catch((error) => {
        console.log(error)
      })

    // Redirect to User List 
    this.props.history.push('/userfacture/' + this.state.userId)
    return ;
  }


  render() {
    return (
      <>
        <Button
          startIcon={<ArrowBackIcon />}
          size="small"
          style={{
            fontSize: 8,
            marginTop: '8%',
            marginLeft: '-10%'
          }}
          variant="contained"
          color="secondary"
          href={"/userfacture/" + this.state.userId}>

        </Button>
        <CSVLink data={this.ExportCSV()} filename={"Note de frais de_" + this.state.username + ".csv"}  >
        <Button
          startIcon={<GetAppIcon />}
          size="md"
          style={{
            fontSize: 8,
            marginTop: '8%',
            marginLeft: '10%'
          }}
          variant="contained"
          color="secondary"
          type="submit"
          onClick={() => {
            { this.ExportCSV() };
            { this.onSubmit() };
          }}
          >
CSV
        </Button>
        </CSVLink>
        <Paper className="paper-dfpa">
          <Card.Img variant="top" src={`http://localhost:8080/${this.state.image}`} />
 
        </Paper>
      
   
    </>);
  }

}
