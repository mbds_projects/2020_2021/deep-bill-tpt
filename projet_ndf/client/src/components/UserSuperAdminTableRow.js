import React, { Component } from 'react';
import axios from 'axios';
import { Button } from '@material-ui/core';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import DetailsIcon from '@material-ui/icons/Details';
import UpdateIcon from '@material-ui/icons/Update';
import Box from '@material-ui/core/Box';
export default class UserSuperAdminTableRow extends Component {

    constructor(props) {
        super(props);
        this.deleteUser = this.deleteUser.bind(this);
    }

    deleteUser() {
        if (window.confirm('Voulez-vous supprimer cet utilisateur?!')) {
            axios.delete('http://localhost:8080/users/test/delete/' + this.props.obj._id)
                .then((res) => {
                    console.log('User successfully deleted!');
                    alert("l'utilisateur a été supprimé");
                }).catch((error) => {
                    console.log(error)
                })


        }
    }


    render() {
       // console.log("props.obj",this.props.obj)
        return (

            <TableRow>
                <TableCell>

                    {this.props.obj.username}

                </TableCell>

                <TableCell width='50%'>
                    <Box display="flex" justifyContent="space-between">



                        < Button
                            startIcon={<UpdateIcon />}
                            size="small"
                            style={{
                                fontSize: 10
                            }}
                            variant="contained"
                            color="primary"
                            href={"/test/superupdate/" + this.props.obj._id}
                        >
                            update role
                        </Button>



                        <Button
                            startIcon={<DetailsIcon />}
                            size="small"
                            style={{
                                fontSize: 10
                            }}
                            variant="contained"
                            color="primary"
                            href={"/userfacturesuperadmin/" + this.props.obj._id}
                        >
                            Liste des Factures
                        </Button>

                        <Button
                            startIcon={<DeleteIcon />}
                            size="small"
                            style={{
                                fontSize: 10
                            }}
                            variant="contained"
                            color="primary"
                           
                            onClick={() => {
                                { this.deleteUser() };
                                { window.location.reload() };
                            }}
                        >
                            Delete user
                        </Button>

                    </Box>


                </TableCell>
            </TableRow>

        );
    }
}