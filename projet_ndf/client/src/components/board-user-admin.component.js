import React, { Component } from "react";
import axios from 'axios';
import Table from 'react-bootstrap/Table';
import UserAdminTableRow from './UserAdminTableRow';
import authHeader from '../services/auth-header';
import AuthService from "../services/auth.service";
import { Card, Button} from 'react-bootstrap';



export default class BoardUserAdmin extends Component {
    constructor(props) {
        super(props);

        this.state = {
            content: "",
            users: []
        };
    }

    componentDidMount() {
        const currentUser = AuthService.getCurrentUser();
        console.log(currentUser.id)

        axios.get('http://localhost:8080/users/test/allUsers', { headers: authHeader() })
            .then(
                response => {
                    this.setState({ users: [...this.state.users, ...response.data] });

                },
                error => {
                    this.setState({
                        content:
                            (error.response &&
                                error.response.data &&
                                error.response.data.message) ||
                            error.message ||
                            error.toString()
                    });
                }
            );
    }
    DataTable() {
        const currentUser = AuthService.getCurrentUser();
        return this.state.users.map((res, i) => {
            //console.log("res:",res._id,currentUser.id)
            if(res._id === currentUser.id){
                return;
            }else {
                return <UserAdminTableRow obj={res} key={i} />;
            }
            
        });
    }

    render() {
        return (
            <div className="row">
                <Card.Body>
                    <Button variant="secondary" href={"../home/"} size="lg" type="submit">
                        retour
                    </Button>

                </Card.Body>
                <Card style={{ width: '100rem' }}>
                    <div className="col table-wrapper">
                        <Table striped bordered hover>
                            <thead style={{
                                borderBottom: 'solid 3px white',
                                background: 'grey',
                                color: 'white',
                                fontWeight: 'bold',
                            }}>
                                <tr>
                                    <th>Username</th>
                                    <th>email</th>
                                    <th>UserId</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.DataTable()}
                            </tbody>
                        </Table>
                        <div
                            ref={loadingRef => (this.loadingRef = loadingRef)} >
                        </div>
                    </div>
                </Card>
            </div>);
    }
};
