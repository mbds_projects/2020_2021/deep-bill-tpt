import React, { Component } from 'react';
import DetailsIcon from '@material-ui/icons/Details';
import { Button } from '@material-ui/core';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
export default class Facture extends Component {

  render() {
    return (



      <TableRow>
        {/* <Card.Img src={`http://localhost:8080/${this.props.obj.image}`} variant='top' /> */}
        <TableCell component="th" scope="row">
        {this.props.obj.name}
        </TableCell>
  
        <TableCell width="35%" component="th" scope="row">

            < Button
              startIcon={<DetailsIcon />}
              size="small"
              style={{
                fontSize: 10
              }}
              variant="contained"
              color="primary"
              href={"/detailpersonnelsuperadmin/" + this.props.obj._id}
            >
              Detail
            </Button>
  
        </TableCell>
        </TableRow>

    );
  }
}

