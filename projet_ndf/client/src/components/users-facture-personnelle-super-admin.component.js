import React, { Component } from "react";
import axios from 'axios';
import {  Button } from '@material-ui/core';
import UserFacturePersonnelleTableSuperAdmin from './UserFacturePersonnelleTableSuperAdmin';
import UserFacturePersonnelleTableSuperAdmin2 from './UserFacturePersonnelleTableSuperAdmin2';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
export default class UsersFacturesPersonnellesSuperAdmin extends Component {

  constructor(props) {
    super(props)
    this.state = {
      factures: []
    };
  }

  getFactures() {
    axios.get(`http://localhost:8080/factures/userfacturepersonnelle/${this.props.match.params.userId}`)
      .then(res => {
        //console.log("je suis le res",res)

        this.setState({ factures: [...this.state.factures, ...res.data] });
      });
  };

  componentDidMount() {
    console.log("componentDidMount")
    this.getFactures(this.state.page);
  }

  DataTable() {
    return this.state.factures.map((res, i) => {
      if (res.traitee === false) {
        return <UserFacturePersonnelleTableSuperAdmin obj={res} key={i} />
      }
    });
  }
  DataTable_length() {
    var table = []
    var k = 0

    this.state.factures.map((res, i) => {
      if (res.traitee === false) {

        table[i] = <UserFacturePersonnelleTableSuperAdmin obj={res} key={i} />
        k++
      }
    });

    return k
  }
  DataTable_header() {
    //var header = <thead><h3>Le client n'a aucune Facture non traitée</h3></thead>
    var header = <Typography
    variant="h4"
    color='secondary'>
    Vous n'avez aucune facture à traiter
  </Typography>
    var header2 = <TableHead >
      <TableRow>
        <TableCell align="left">Nom de la Facture</TableCell>
        <TableCell align="left">Action</TableCell>
      </TableRow>
    </TableHead>

    if (this.DataTable_length() === 0) {
      return header
    } else {
      return header2
    }

  }

  DataTable2() {
    var table2 = []

    this.state.factures.map((res, i) => {
      if (res.traitee === true) {

        table2[i] = <UserFacturePersonnelleTableSuperAdmin2 obj={res} key={i} />

      }
    });
    return table2
  }
  DataTable2_length() {
    var table2 = []
    var k = 0

    this.state.factures.map((res, i) => {
      if (res.traitee === true) {

        table2[i] = <UserFacturePersonnelleTableSuperAdmin2 obj={res} key={i} />
        k++
      }
    });

    return k
  }
  DataTable2_header() {
    //var header = <thead><h3>Le client n'a aucune facture traitée</h3></thead>
    var header = <Typography
    variant="h4"
    color='secondary'>
    Vous n'avez aucune facture traitée
  </Typography>
    var header2 = <TableHead >
      <TableRow>
        <TableCell align="left">Nom de la Facture</TableCell>
        <TableCell align="left">Action</TableCell>
      </TableRow>
    </TableHead>


    if (this.DataTable2_length() === 0) {
      return header
    } else {
      return header2
    }

  }

  render() {
    //console.log("datatable: ",this.DataTable())
    //console.log("datatable2: ",this.DataTable2())
    //console.log("datatable: ",this.DataTable().length)
    console.log("datatable_length: ", this.DataTable_length())
    console.log("datatable2_length: ", this.DataTable2_length())
    return (
      <>
        <Button
          startIcon={<ArrowBackIcon />}
          size="small"
          style={{
            fontSize: 8,
            marginLeft: '-10%',
            marginTop: '8%'
          }}
          variant="contained"
          color="secondary"
          href={"../home"}>

        </Button>
        
        <div className="bar-table">
        <Typography
            variant="h5"
            color='primary'
            style={{ marginTop: '50px', marginLeft: '100px' }}

          >
            Espace de vos factures

          </Typography>
        <Typography
          variant="h7"
          color='primary'
          style={{ marginTop: '0px', marginLeft: '100px' }}>
          Suivez l'état de traitement de chaque facture
        </Typography>

          <TableContainer component={Paper} className="table-container" >
            <Typography
              variant='h5'
              color='primary'

            >
              La liste des factures non traitées :
            </Typography>
            <br />
            <Table size="medium" aria-label="a dense table">
              {this.DataTable_header()}
              <TableBody>
                {this.DataTable()}
              </TableBody>
            </Table>
          </TableContainer>

          <TableContainer component={Paper} className="table-container" >
            <Typography
              variant='h5'
              color='primary'

            >
              La liste des factures traitées :
            </Typography>
            <br />
            <Table size="medium" aria-label="a dense table">
              {this.DataTable2_header()}
              <TableBody>
                {this.DataTable2()}
              </TableBody>
            </Table>
          </TableContainer>
          <div
                ref={loadingRef => (this.loadingRef = loadingRef)} >
              </div>

        </div>



      </>
    );


    /*  return (
        
        <div className="row">  
            
           <br/><br/>
           <div>
       <Button variant="danger" href={"../user/"} size="lg" block="block" type="submit">
                    retour
                  </Button>
         </div>
         <br/><br/>
           <div>
           <h2>Espace de l'utilisateur</h2>
           </div>
           
         
        
           
          
       <div>
        <div className="col table-wrapper">
       
          <Table striped bordered hover>
          <thead>
            <tr><b>Factures non Traitées</b></tr>
            <tr>
              <th>Nom de l'utilisateur</th>
              <th>Nom de la Facture</th>
              <th>Montant</th>
              <th>Action</th>
            </tr>
          </thead>
            <tbody>
              {this.DataTable()}
            </tbody>
          </Table>
          
    <div
            ref={loadingRef => (this.loadingRef = loadingRef)} >
          </div>
        </div>
    
        <br/><br/>
        <div className="col table-wrapper">
       
          <Table striped bordered hover>
          <thead>
              <tr> <b>Factures Traitées</b></tr>
              <tr>
              <th>Nom de l'utilisateur</th>
                <th>Nom de la Facture</th>
                <th>Montant</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {this.DataTable2()}
            </tbody>
          </Table>
          <div
            ref={loadingRef => (this.loadingRef = loadingRef)} >
          </div>
        </div>
        </div>
        </div>
      
    );*/
  }


}