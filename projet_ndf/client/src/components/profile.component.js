import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import AuthService from "../services/auth.service";
import CreateFacture from "./create-facture.component";
import { Card} from 'react-bootstrap';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { Button } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
export default class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      redirect: null,
      userReady: false,
      currentUser: { username: "" }
    };
  }

  componentDidMount() {
    const currentUser = AuthService.getCurrentUser();

    if (!currentUser) this.setState({ redirect: "/home" });
    this.setState({ currentUser: currentUser, userReady: true })
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />
    }


    return (
      <div className="col-md-12">
        <Button
                        startIcon={<ArrowBackIcon />}
                        size="small"
                        style={{
                            fontSize: 8,
                            marginLeft: '-10%',
                            marginTop: '8%'
                        }}
                        variant="contained"
                        color="secondary"
                        href={"../home"}>

                    </Button>
        <Card style={{ width: '50rem', marginLeft: '20%', marginTop: '0%' }}>
        <Typography
          variant="h4"
          color='primary'
          style={{ marginTop: '40px', marginLeft: '180px',marginBottom: '20%' }}>
          Envoyer vos factures
        </Typography>
          
            {(this.state.userReady) ?
              <div >
                <CreateFacture />
              </div> : null}
          
        </Card>
      </div>
    );
  }
}
