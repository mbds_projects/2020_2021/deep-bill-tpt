import React, { Component } from "react";
import { Form } from 'react-bootstrap';
import { Button } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Paper from '@material-ui/core/Paper';
import axios from 'axios';
import UpdateIcon from '@material-ui/icons/Update';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';
let assistantId, userId, adminId, superadminId, assistant, user, admin, superadmin

export default class EditUser extends Component {

  constructor(props) {
    super(props)
    this.onChangeUserUsername = this.onChangeUserUsername.bind(this);
    this.onChangeUserRoles = this.onChangeUserRoles.bind(this);
    this.onChangeUserEmail = this.onChangeUserEmail.bind(this);
    this.onChangeRoleName = this.onChangeRoleName.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    // State
    this.state = {
      username: '',
      roles: '',
      email: '',
      name: '',
      userRoles: [],
      le_role: ''
    }
  }

  getRoles(page) {
    axios.get('http://localhost:8080/users/test/AllRoles')
      .then(resp => {
        console.log(resp)
        this.setState({ userRoles: [...this.state.userRoles, ...resp.data] });
      });
      console.log("this.state.userRoles1", this.state.userRoles.length)
  }

  getUsers(page) {
    axios.get('http://localhost:8080/users/test/edit/' + this.props.match.params.id)
      .then(res => {
        this.setState({
          username: res.data.username,
          roles: res.data.roles,
          email: res.data.email,
          le_role: res.data.roles
        });
        console.log("this.state.userRoles2", this.state.userRoles)
        var allroles = this.state.userRoles.map(r => {
          return {
            name: r.name,
            _id: r._id
          }
        })

        for (let i = 0; i < allroles.length; i++) {
          if (allroles[i].name === "assistant") {
              assistant = allroles[i].name
              assistantId = allroles[i]._id
          } else if (allroles[i].name === "admin") {
              admin = allroles[i].name
              adminId = allroles[i]._id
          } else if (allroles[i].name === "superadmin") {
              superadmin = allroles[i].name
              superadminId = allroles[i]._id
          } else {
              user = allroles[i].name
              userId = allroles[i]._id
          }
      };

      if (this.state.roles == userId) {
        this.setState({
            name: user
        });
    } else if (this.state.roles == adminId) {
        this.setState({
            name: admin
        });
    } else if (this.state.roles == superadminId) {
        this.setState({
            name: superadmin
        });
    } else if (this.state.roles == assistantId) {
        this.setState({
            name: assistant
        });
    }
      })
      .catch((error) => {
        console.log(error);
      })
  };

  componentDidMount() {
    this.getRoles();
    this.getUsers(this.state.page);
  }

  onChangeUserUsername(e) {
    this.setState({ username: e.target.value })
  }

  onChangeUserRoles(e) {
    this.setState({ roles: e.target.value })
  }

  onChangeUserEmail(e) {
    this.setState({ email: e.target.value })
  }

  onChangeRoleName(e) {

    this.setState({ name: e.target.value })

    var allroles = this.state.userRoles.map(r => {
      return {
        name: r.name,
        _id: r._id
      }
    })
    for (let i = 0; i < allroles.length; i++) {
      if (allroles[i].name === "assistant") {
          assistant = allroles[i].name
          assistantId = allroles[i]._id
      } else if (allroles[i].name === "admin") {
          admin = allroles[i].name
          adminId = allroles[i]._id
      } else if (allroles[i].name === "superadmin") {
          superadmin = allroles[i].name
          superadminId = allroles[i]._id
      } else {
          user = allroles[i].name
          userId = allroles[i]._id
      }
  };

  switch (e.target.value) {
    case "assistant":
        this.setState({ roles: assistantId })
        break
    case "user":
        this.setState({ roles: userId })
        break
    case "admin":
        this.setState({ roles: adminId })
        break
    case "superadmin":
        this.setState({ roles: superadminId })
        break
    case "":
        this.setState({ roles: this.state.le_role })
        break
}
  }
  listUsers = () => {

    var list = this.state.userRoles.map(r => (
        
        <option
            key={r._id}
            value={r.name}>
            {r.name}
        </option>

    ));
    
    var r = [];
    var k = 0
    if (list != '') {
        for (var i = 0; i < list.length; i++) {
            if (list[i].props.value != superadmin && list[i].props.value != admin) {
                 //console.log("list", list[i].props)
                r[k] = <>
                    <option
                        key={list[i].props.value}
                        value={list[i].props.value}>
                        {list[i].props.children}
                    </option></>
                k++
            }

        }
    }
    r = <> <option aria-label="None" value="" /> + {r}</>

    return r
}
  onSubmit(e) {
    e.preventDefault()
    const userObject = {
      username: this.state.username,
      roles: this.state.roles,
      email: this.state.email
    };

    axios.put('http://localhost:8080/users/test/update/' + this.props.match.params.id, userObject)
      .then((res) => {
        console.log(res.data)
        console.log('User successfully updated')
      }).catch((error) => {
        console.log(error)
      })
    this.props.history.push('/admin')
  }


  render() {
    return (
      <>
        <Button
          startIcon={<ArrowBackIcon />}
          size="small"
          style={{
            fontSize: 8,
            marginTop: '8%',
            marginLeft: '-10%'
          }}
          variant="contained"
          color="secondary"
          href={"/admin"}>

        </Button>
        <Typography
          variant="h5"
          color='primary'
          style={{ marginTop: '0%', marginLeft: '20%' }}
        >
          Espace de modification du role :
        </Typography>
        <Paper className="paper-eu">
          <Form onSubmit={this.onSubmit} style={{ marginTop: '5%', marginLeft: '20%' }}>
            <Form.Group controlId="Name" >
              <Form.Label><b>Username :</b>  {this.state.username}</Form.Label>
            </Form.Group>

            <Form.Group controlId="Name">
              <Form.Label><b> Role : </b>{this.state.name}</Form.Label>
            </Form.Group>


            <div class="form-group">

              <FormControl variant="filled" onChange={this.onChangeRoleName} multiple style={{ marginBottom: '2%', marginLeft: '5%', width: '40%' }}>
                <InputLabel htmlFor="filled-age-native-simple" >Role</InputLabel>
                <Select
                  native

                >
                  {this.listUsers()}
                </Select>
              </FormControl>
            </div>
            <Button
              startIcon={<UpdateIcon />}
              size="md"
              style={{
                fontSize: 10,
                marginTop: 20,
                marginLeft: 50
              }}
              variant="contained"
              color="secondary"
              type="submit"
            >
              Update Role
            </Button>
          </Form>
        </Paper>

      </>);
  }
}
