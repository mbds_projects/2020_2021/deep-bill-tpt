import React, { Component } from "react";
import { Grid, Button } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';



export default class SideBar_retour_ufpa extends Component {
    render() {
        return (
            <div className="side-bar-retour">
                <Grid container>
                    
                    <Grid item sm={8} style={{marginTop: '80%'}}>
                    <Button
                        startIcon={<ArrowBackIcon />}
                        size="small"
                        style={{
                            fontSize: 8
                        }}
                        variant="contained"
                        color="secondary"
                        href={"../home"}>

                    </Button>
                        
                    </Grid>               
                </Grid>


            </div>
        )
    }



}