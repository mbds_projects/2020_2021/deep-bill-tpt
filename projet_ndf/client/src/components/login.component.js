import React, { Component } from "react";
import Form from "react-validation/build/form";
import CheckButton from "react-validation/build/button";
import TextField from '@material-ui/core/TextField';
import AuthService from "../services/auth.service";
import Paper from '@material-ui/core/Paper';
import { Card } from 'react-bootstrap';
import '../App.css'
const required = value => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.handleLogin = this.handleLogin.bind(this);
    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);

    this.state = {
      username: "",
      password: "",
      loading: false,
      message: ""
    };
  }

  onChangeUsername(e) {
    this.setState({
      username: e.target.value
    });
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value
    });
  }

  handleLogin(e) {
    e.preventDefault();

    this.setState({
      message: "",
      loading: true
    });

    this.form.validateAll();

    if (this.checkBtn.context._errors.length === 0) {
      AuthService.login(this.state.username, this.state.password).then(
        () => {
          this.props.history.push("/home");
          window.location.reload();
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          this.setState({
            loading: false,
            message: resMessage
          });
        }
      );
    } else {
      this.setState({
        loading: false
      });
    }
  }

  render() {
    return (
      <>
      
      <Paper className="paper-login">
      <Card.Img variant="top" 
      src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" 
      className="profile-img-card"
      
      />
 

          <Form
            onSubmit={this.handleLogin}
            ref={c => {
              this.form = c;
            }}
          >
            
            <div className="form-group" style={{marginTop: '80px', marginLeft: '120px'}}>
            
              <TextField
                variant="filled"
                color="secondary"
                type="text"
                id="username"
                label="Username"
                InputLabelProps={{ shrink: true, margin: "dense" }}
                value={this.state.username}
                onChange={this.onChangeUsername}
                validations={[required]}
              />
            </div>

            <div className="form-group" style={{marginTop: '20px' , marginLeft: '120px'}}>

              <TextField
                variant="filled"
                color="secondary"
                type="password"
                id="password"
                label="Password"
                InputLabelProps={{ shrink: true, margin: "dense" }}
                value={this.state.password}
                onChange={this.onChangePassword}
                validations={[required]}
              />
            </div>

            <div className="form-group" style={{marginTop: '40px' , marginLeft: '160px'}}>
            <button
                style={{width: '100px'}}
                className="btn btn-primary btn-block"
                disabled={this.state.loading}
              >
                {this.state.loading && (
                  <span className="spinner-border spinner-border-sm"></span>
                )}
                <span>Login</span>
              </button>
             
            </div>

            {this.state.message && (
              <div className="form-group">
                <div className="alert alert-danger" role="alert">
                  {this.state.message}
                </div>
              </div>
            )}
            <CheckButton
              style={{ display: "none" }}
              ref={c => {
                this.checkBtn = c;
              }}
            />
          </Form>
      </Paper>
      
      </>
    );
  }
}
