import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import AuthService from "../services/auth.service";
import BoardAssistant from "./board-assistant.component";
import BoardAdmin from "./board-admin.component";
import axios from 'axios';

let assistantId, userId, adminId, assistant, user, admin
export default class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      redirect: null,
      userReady: false,
      currentUser: { username: "" },
      userRoles: [],
      r: ''
    };
  }

  getRoles(page) {
    axios.get('http://localhost:8080/users/test/AllRoles')
      .then(resp => {
        console.log(resp)
        this.setState({ userRoles: [...this.state.userRoles, ...resp.data] });
      });
  }
  componentDidMount() {
    const currentUser = AuthService.getCurrentUser();
    this.getRoles();
    if (!currentUser) this.setState({ redirect: "/home" });
    this.setState({ currentUser: currentUser, userReady: true })
  }

  espacePersonnel() {
    var allroles = this.state.userRoles.map(r => {
      return {
        name: r.name,
        _id: r._id
      }
    })
    for (let i = 0; i < allroles.length; i++) {
      if (allroles[i].name === "assistant") {
        assistant = allroles[i].name
        assistantId = allroles[i]._id
      } else if (allroles[i].name === "admin") {
        admin = allroles[i].name
        adminId = allroles[i]._id
      } else {
        user = allroles[i].name
        userId = allroles[i]._id
      }
    };

    switch (this.setState.r) {
      case "assistant":
        this.setState({ r: 1 })
        break
      case "user":
        this.setState({ r: 2 })
        break
      case "admin":
        this.setState({ r: 3 })
        break
    }

  }

  espace(v){
    if(v === 1){
      return BoardAssistant
    }else if(v===2){
      return BoardAdmin
    }

  }

  
  

  render() {
    
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />
    }
  

    return (
     
        <div className="container">
          {this.espace(this.espacePersonnel())}
        </div>
     
    );
  }
}
