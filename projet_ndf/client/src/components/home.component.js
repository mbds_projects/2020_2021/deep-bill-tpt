import React, { Component } from "react";
import { Button } from '@material-ui/core';
import AuthService from "../services/auth.service";
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import AssistantIcon from '@material-ui/icons/Assistant';
import SendIcon from '@material-ui/icons/Send';
import DetailsIcon from '@material-ui/icons/Details';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import "../App.css";
export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {

      showAssistantBoard: false,
      showAdminBoard: false,
      showSuperAdminBoard: false,
      showUserBord: false,
      currentUser: undefined,
    };
  }

  componentDidMount() {
    const user = AuthService.getCurrentUser();
    if (user) {
      this.setState({
        currentUser: user,
        showAssistantBoard: user.roles.includes("ROLE_ASSISTANT"),
        showAdminBoard: user.roles.includes("ROLE_ADMIN"),
        showSuperAdminBoard: user.roles.includes("ROLE_SUPERADMIN"),
        showUserBord: user.roles.includes("ROLE_USER"),
      });
    }

  }

  render() {
    const { currentUser, showAssistantBoard, showAdminBoard, showSuperAdminBoard, showUserBord } = this.state;
    if (this.state.currentUser) {
      console.log("currentuser", this.state.currentUser.roles)
    }

    return (

      <>
        {currentUser && (
          <Typography
            variant="h5"
            color='primary'
            style={{ marginTop: '10%', marginLeft: '20%' }}

          >
            Bonjour {currentUser.username}

          </Typography>)}
        <Typography
          variant="h7"
          color='primary'
          style={{ marginTop: '0%', marginLeft: '20%' }}>
          Bienvenue dans votre Espace
        </Typography>


        <TableContainer component={Paper} className="table-container-home" >
          <Table size="medium" aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="left">Consultez vos factures :</TableCell>
                <TableCell align="left">Envoyez vos factures </TableCell>
                {showAssistantBoard && (
                  <TableCell align="left"> Accédez à l'espace de gestion des clients :</TableCell>)}
                {showAdminBoard && (
                  <TableCell align="left"> Accédez à l'espace de gestion des clients et assistantes :</TableCell>)}
                {showSuperAdminBoard && (
                  <TableCell align="left"> Accédez à l'espace de gestion des utilisateurs :</TableCell>)}

              </TableRow>
            </TableHead>
            <TableBody>

              <TableRow >
                <TableCell align="left">
                  {showAssistantBoard && (




                    <>



                      < Button
                        startIcon={<DetailsIcon />}
                        size="lg"
                        style={{
                          fontSize: 10,
                          marginTop: 50,
                          marginLeft: 50,
                          marginBottom: 20
                        }}
                        variant="contained"
                        color="primary"

                        href={"/userfacturepersonnelleassistant/" + `${currentUser.id}`}
                      >
                        Mes Factures
                      </Button>
                    </>


                  )}
                  {showAdminBoard && (




                    <>



                      < Button
                        startIcon={<DetailsIcon />}
                        size="lg"
                        style={{
                          fontSize: 10,
                          marginTop: 50,
                          marginLeft: 50,
                          marginBottom: 20
                        }}
                        variant="contained"
                        color="primary"

                        href={"/userfacturepersonnelleadmin/" + `${currentUser.id}`}
                      >
                        Mes Factures
                      </Button>
                    </>


                  )}
                  {showSuperAdminBoard && (




                    <>



                      < Button
                        startIcon={<DetailsIcon />}
                        size="lg"
                        style={{
                          fontSize: 10,
                          marginTop: 50,
                          marginLeft: 50,
                          marginBottom: 20
                        }}
                        variant="contained"
                        color="primary"

                        href={"/userfacturepersonnellesuperadmin/" + `${currentUser.id}`}
                      >
                        Mes Factures
                      </Button>
                    </>


                  )}
                  {showUserBord && (




                    <>



                      < Button
                        startIcon={<DetailsIcon />}
                        size="lg"
                        style={{
                          fontSize: 10,
                          marginTop: 50,
                          marginLeft: 50,
                          marginBottom: 20
                        }}
                        variant="contained"
                        color="primary"

                        href={"/userfacturepersonnelle/" + `${currentUser.id}`}
                      >
                        Mes Factures
                      </Button>
                    </>


                  )}
                </TableCell>
                <TableCell align="left">
                  {showAssistantBoard && (



                    <>


                      < Button
                        startIcon={<SendIcon />}
                        size="lg"
                        style={{
                          fontSize: 10,
                          marginTop: 50,
                          marginLeft: 50,
                          marginBottom: 20
                        }}
                        variant="contained"
                        color="primary"

                        href={"/profile"}
                      >
                        envoyer mes factures
                      </Button>
                    </>



                  )}
                  {showAdminBoard && (



                    <>


                      < Button
                        startIcon={<SendIcon />}
                        size="lg"
                        style={{
                          fontSize: 10,
                          marginTop: 50,
                          marginLeft: 50,
                          marginBottom: 20
                        }}
                        variant="contained"
                        color="primary"

                        href={"/profile"}
                      >
                        envoyer mes factures
                      </Button>
                    </>



                  )}
                  {showSuperAdminBoard && (



                    <>


                      < Button
                        startIcon={<SendIcon />}
                        size="lg"
                        style={{
                          fontSize: 10,
                          marginTop: 50,
                          marginLeft: 50,
                          marginBottom: 20
                        }}
                        variant="contained"
                        color="primary"

                        href={"/profile"}
                      >
                        envoyer mes factures
                      </Button>
                    </>



                  )}
                  {showUserBord && (



                    <>


                      < Button
                        startIcon={<SendIcon />}
                        size="lg"
                        style={{
                          fontSize: 10,
                          marginTop: 50,
                          marginLeft: 50,
                          marginBottom: 20
                        }}
                        variant="contained"
                        color="primary"

                        href={"/profile"}
                      >
                        envoyer mes factures
                      </Button>
                    </>



                  )}
                </TableCell>
                <TableCell align="left">
                  {showAssistantBoard && (


                    <>

                      < Button
                        startIcon={<AssistantIcon />}
                        size="lg"
                        style={{
                          fontSize: 10,
                          marginTop: 50,
                          marginLeft: 50,
                          marginBottom: 20
                        }}
                        variant="contained"
                        color="primary"

                        href={"/assistant"}
                      >
                        Assistant Board
                      </Button>
                    </>
                  )}
                  {showAdminBoard && (


                    <>

                      < Button
                        startIcon={<AssistantIcon />}
                        size="lg"
                        style={{
                          fontSize: 10,
                          marginTop: 50,
                          marginLeft: 50,
                          marginBottom: 20
                        }}
                        variant="contained"
                        color="primary"

                        href={"/admin"}
                      >
                        Admin Board
                      </Button>
                    </>
                  )}
                  {showSuperAdminBoard && (


<>

  < Button
    startIcon={<AssistantIcon />}
    size="lg"
    style={{
      fontSize: 10,
      marginTop: 50,
      marginLeft: 50,
      marginBottom: 20
    }}
    variant="contained"
    color="primary"

    href={"/superadmin"}
  >
  Super  Admin Board
  </Button>
</>
)}
                </TableCell>


              </TableRow>

            </TableBody>
          </Table>
        </TableContainer>






      </>

    );
  }
}
