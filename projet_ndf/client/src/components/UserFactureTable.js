import React, { Component } from 'react';
import { Button } from '@material-ui/core';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import SaveIcon from '@material-ui/icons/Save';
import DetailsIcon from '@material-ui/icons/Details';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Box from '@material-ui/core/Box';
export default class Facture extends Component {

  render() {
    //console.log("this.props",this.props.obj)
    var nowDate = new Date(this.props.obj.createdAt);
    var year = nowDate.getFullYear();
    var month = nowDate.getMonth() + 1;
    var day = nowDate.getDate();
    var date = year + '-' + month + '-' + day;
    return (


<TableRow>
     
        {/* <Card.Img src={`http://localhost:8080/${this.props.obj.image}`} variant='top' /> */}
        
       
        <TableCell component="th" scope="row">
        {this.props.obj.name}
        </TableCell>
        <TableCell component="th" scope="row">
        {date}
        </TableCell>

        <TableCell align="center" component="th" scope="row">
        
               <Box display="flex" justifyContent="space-between">



            < Button
              startIcon={<DetailsIcon />}
              size="small"
              style={{
                fontSize: 10
              }}
              variant="contained"
              color="primary"
              href={"/detail/" + this.props.obj._id}
            >
              Detail
            </Button>




            < Button
              startIcon={<EditIcon />}
              size="small"
              style={{
                fontSize: 10
              }}
              variant="contained"
              color="primary"
              href={"/edit/" + this.props.obj._id}
            >
              Edit
            </Button>






            <Button
              startIcon={<DeleteIcon />}
              size="small"
              style={{
                fontSize: 10
              }}
              variant="contained"
              color="primary"
              href={"/delete/" + this.props.obj._id}
            >
              Delete
            </Button>



            < Button
              startIcon={<SaveIcon />}

              size="small"
              style={{
                fontSize: 10
              }}
              variant="contained"
              color="primary"
              href={"/update/" + this.props.obj._id}
            >
              Note de Frais
            </Button>




          </Box>
              
        </TableCell>
        
      
      </TableRow>
    );
  }
}

