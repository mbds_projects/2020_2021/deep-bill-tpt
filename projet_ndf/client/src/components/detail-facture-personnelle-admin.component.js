import React, { Component } from "react";
import { Card } from 'react-bootstrap';
import axios from 'axios';
import {  Button } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Paper from '@material-ui/core/Paper';
import '../App.css'

export default class DetailFacturePersonnelleAdmin extends Component {

  constructor(props) {
    super(props)
  
    this.onSubmit = this.onSubmit.bind(this);
var k=0;
    // State
    this.state = {
      name: '',
      montant: '',
      image: ''
    }
  }

  componentDidMount() {
    axios.get('http://localhost:8080/factures/detailpersonnel/' + this.props.match.params.id)
      .then(res => {
     
        this.setState({
          name: res.data.name,
          montant: res.data.montant,
          image: res.data.image,
          userId: res.data.userId
          
        });
       
      })
      .catch((error) => {
        console.log(error);
      })
  }



  
  onSubmit(e) {
    this.props.history.push('/facture-list')
  }


  render() {
    return (
      <>
      <Button
                          startIcon={<ArrowBackIcon />}
                          size="small"
                          style={{
                              fontSize: 8,
                              marginTop: '8%',
                              marginLeft: '-10%'
                          }}
                          variant="contained"
                          color="secondary"
                          href={"/userfacturepersonnelleadmin/" + this.state.userId}>
  
                      </Button>
      
  
        
  
       
      <Paper className="paper-dfpa">
        <Card.Img variant="top" src={`http://localhost:8080/${this.state.image}`} />
        </Paper>
  
      
      </>
      );
  }
}
