import React, { Component } from "react";
import axios from 'axios';
import UserAdminTableRow from './UserAdminTableRow';
import authHeader from '../services/auth-header';
import AuthService from "../services/auth.service";
import { Button } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Typography from '@material-ui/core/Typography';
export default class BoardAdmin extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: "",
      users: [],
      userRoles: [],
      assistant: '',
      assistantId: '',
      userId: '',
      adminId: '',
      superadminId: '',
      user: '',
      admin: '',
      superadmin: '',
      finaltable: []
    };
  }
  getRoles(page) {
    axios.get('http://localhost:8080/users/test/AllRoles')
      .then(resp => {
        //console.log(resp)
        this.setState({ userRoles: [...this.state.userRoles, ...resp.data] });
        var allroles = this.state.userRoles.map(r => {
          return {
            name: r.name,
            _id: r._id
          }
        })
        //console.log("allroles", allroles)
        for (let i = 0; i < allroles.length; i++) {
          if (allroles[i].name === "assistant") {
            this.state.assistant = allroles[i].name
            this.state.assistantId = allroles[i]._id
          } else if (allroles[i].name === "admin") {
            this.state.admin = allroles[i].name
            this.state.adminId = allroles[i]._id
          } else if (allroles[i].name === "superadmin") {
            this.state.superadmin = allroles[i].name
            this.state.superadminId = allroles[i]._id
          } else {
            this.state.user = allroles[i].name
            this.state.userId = allroles[i]._id
          }

          //console.log("assistant", this.state.assistant)
        };
      });
    //console.log("this.state.userRoles1", this.state.userRoles)
  }
  getUsers(page) {
    axios.get('http://localhost:8080/users/test/allUsers', { headers: authHeader() })
      .then(
        response => {
          this.setState({ users: [...this.state.users, ...response.data] });
          //console.log("this.state.userRoles2", this.state.userRoles)

        },
        error => {
          this.setState({
            content:
              (error.response &&
                error.response.data &&
                error.response.data.message) ||
              error.message ||
              error.toString()
          });
        }
      );
  }
  componentDidMount() {
    const currentUser = AuthService.getCurrentUser();
    // console.log(currentUser.id)
    this.getRoles()
    this.getUsers(this.state.page)

  }
  DataTable() {
    const currentUser = AuthService.getCurrentUser();
    return this.state.users.map((res, i) => {
      console.log("res:",res.roles[0])
      //console.log("this.state.superadminId:",this.state.superadminId)
      if (res._id === currentUser.id || res.roles[0] === this.state.superadminId || res.roles[0] === this.state.adminId) {
        return;
      } else  {
        
        return <UserAdminTableRow obj={res} key={i} />;
      }

    });
  }
 
  render() {
    return (
      <>
        <Button
          startIcon={<ArrowBackIcon />}
          size="small"
          style={{
            fontSize: 8,
            marginLeft: '-10%',
            marginTop: '8%'
          }}
          variant="contained"
          color="secondary"
          href={"../home"}>

        </Button>
        <Typography
          variant="h5"
          color='primary'
          style={{ marginTop: '0%', marginLeft: '30%' }}
        >
          Espace de gestion des utilisateurs :
        </Typography>

        <TableContainer component={Paper} className="table-container-home" >

          <Table size="medium" aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="left">Nom de l'utilisateur :</TableCell>
                <TableCell align="center">Action :</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.DataTable()}
            </TableBody>
          </Table>
        </TableContainer>
      </>);
  }
}
