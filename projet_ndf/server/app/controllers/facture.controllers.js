const axios = require("axios")
const Facture = require('../models/facture.model')
var ObjectId = require('mongodb').ObjectID;

exports.createFacture = (req, res) => {
  let userId = req.body.userId
  let username = req.body.username
  let name = req.body.name
  let image = req.file.path
  //let montant = req.file.montant
  //console.log(name, image,userId,req)
  //console.log( "je suis l'url de l'image",image);
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////
  const path = require('path')
  const { spawn } = require('child_process')
  const fs = require('fs')
  const data64 = fs.readFileSync(image, 'base64')
  //console.log(data64);
  var img = "hi i went to python, and i'm back now"
  /**
   * Run python script, pass in `-u` to not buffer console output 
   * @return {ChildProcess}
   */
  var r = spawn('python', [
    "-u",
    path.join(__dirname, 'script.py'),
    image,
  ]);
  r.stdin.write(data64);
  r.stdin.end();
  function runScript() {
    return r
  }

  const subprocess = runScript()
  var m, m1, mht, t;
  var traitee = false;
  var categorie;
 
  

  // print output of script
  subprocess.stdout.on('data', (data) => {
    //console.log(`${data}`);
    //console.log("je suis la data",data);
    value = data.toString();
    //value = value.replace(',', '.');
    //console.log("me voilà ",value)
    //console.log("\n",isNaN(value))
    var arrayOfLines = value.split("\n");


   /* 
    switch (categorie) {
      case "restauration":
        categories = ObjectId("611044e21824684435768468");
        break
      case "Commerce de détail":
        categories = ObjectId("611044e21824684435768469");
        break
      case "transports":
        categories = ObjectId("611044e2182468443576846a");
        break
      case "hebergement":
        categories = ObjectId("611044e2182468443576846b");
        break
      case "autre":
        categories = ObjectId("611044e2182468443576846c");
        break
    }
*/
    if (isNaN(arrayOfLines[0]) === true) {
      m = 0;
    } else {

      //str=str.replace(',', '.');
      m = parseFloat(arrayOfLines[0]);
    }

    if (isNaN(arrayOfLines[1]) === true) {
      t = 0;
    } else {

      //str=str.replace(',', '.');

      t = parseFloat(arrayOfLines[1]);
    }

    m1 = m-t;
    
    if(m1<0) {
      m1=0
       }
       //m1 =m1.toFixed(2);
    mht= Math.round(m1 * 100) / 100
    console.log("je suis le montant ht",mht);
    categorie = arrayOfLines[2]
    

    //console.log("\n", m);
    console.log("je suis la catégorie",categorie);
  });
  subprocess.stderr.on('data', (data) => {
    console.log(`error:${data}`);
  });
  subprocess.on('close', () => {
    //console.log("Closed");

    //console.log("je suis le type du montant", typeof m);
    //console.log("\nje suis le type du traitee", typeof traitee);
    const facture = new Facture({
      userId: userId,
      categories: categorie,
      username: username,
      name: name,
      image: image,
      montant: m,
      tva: t,
      montant_ht: mht,
      traitee: traitee

    })
    facture.save((err, facture) => {
      if (err) {
        console.log(err)
        return res.status(400).json({
          errors: err.meesage
        })
      }
      return res.json({
        message: "Created facture successfully",
        facture
      })
    })

  });

  //console.log("\n je suis le montant",subprocess);
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /* const facture = new Facture({
         userId: userId,
         name: name,
         image: image
         //montant: montant
     })
     facture.save((err, facture) => {
         if (err) {
             console.log(err)
             return res.status(400).json({
                 errors: err.meesage
             })
         }
         return res.json({
             message: "Created facture successfully",
             facture
         })
     }) */

}


exports.getOneFacture = (req, res, next) => {

  Facture.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
}

exports.modifyFacture = (req, res, next) => {
  Facture.findByIdAndUpdate(req.params.id, {
    $set: req.body
  }, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.json(data)
      console.log('Facture updated successfully !')
    }
  })
};

exports.deleteFacture = (req, res, next) => {
  Facture.findByIdAndRemove(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({
        msg: data
      })
    }
  })
};

;

exports.getAllFacture = (req, res, next) => {
  Facture.find().then(
    (factures) => {
      res.status(200).json(factures);
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.getUserFactures = (req, res, next) => {
  const userId = req.params.userId
  //console.log("userId",userId)
  Facture.find({ userId }).then(
    (factures) => {
      res.status(200).json(factures);
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.getUserNotTreatFactures = (req, res, next) => {
  const userId = req.params.userId
  //console.log("userId",userId)
  Facture.find({ userId: userId, traitee: false }).then(
    (factures) => {
      res.status(200).json(factures);
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.CreateUserFacture = (req, res) => {
  let userId = req.body.userId
  let username = req.body.username
  let name = req.body.name
  let image = req.file.path

  const path = require('path')
  const { spawn } = require('child_process')
  const fs = require('fs')
  const data64 = fs.readFileSync(image, 'base64')
  //console.log(data64);
  var img = "hi i went to python, and i'm back now"

  var r = spawn('python', [
    "-u",
    path.join(__dirname, 'script.py'),
    image,
  ]);
  r.stdin.write(data64);
  r.stdin.end();
  function runScript() {
    return r
  }

  const subprocess = runScript()
  var m, m1, t;
  var traitee = false;
  var categorie = 'autre';
  var categories;
  
  //var autreId = new ObjectID();
 




  // print output of script
  subprocess.stdout.on('data', (data) => {

    value = data.toString();
    //value = value.replace(',', '.');

    
    var arrayOfLines = value.split("\n");
   /*  switch (categorie) {
      case "restauration":
        categories = ObjectId("610407ed4b6e294e9c12fc05");
        break
      case "Commerce de détail":
        categories = ObjectId("610407ed4b6e294e9c12fc06");
        break
      case "transports":
        categories = ObjectId("611044e2182468443576846a");
        break
      case "hebergement":
        categories = ObjectId("610407ed4b6e294e9c12fc08");
        break
      case "autre":
        categories = ObjectId("610407ed4b6e294e9c12fc09");
        break
    }*/


    if (isNaN(arrayOfLines[0]) === true) {
      m = 0;
    } else {

      //str=str.replace(',', '.');
      m = parseFloat(arrayOfLines[0]);
    }
    if (isNaN(arrayOfLines[1]) === true) {
      t = 0;
    } else {

      //str=str.replace(',', '.');

      t = parseFloat(arrayOfLines[1]);
    }

    if (m > t) {
      m1 = m - t;
    } else {
      t = 0;
      m1 = 0
    }



  });
  subprocess.stderr.on('data', (data) => {
    console.log(`error:${data}`);
  });

  subprocess.on('close', () => {
    const facture = new Facture({
      userId: userId,
      categories: categorie,
      username: username,
      name: name,
      image: image,
      montant: m,
      tva: t,
      montant_ht: m1,
      traitee: traitee
    })
    facture.save((err, facture) => {
      if (err) {
        console.log(err)
        return res.status(400).json({
          errors: err.meesage
        })
      }
      return res.json({
        message: "Created facture successfully",
        facture
      })
    })
  });
}