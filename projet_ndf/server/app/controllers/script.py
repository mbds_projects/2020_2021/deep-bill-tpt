#!/usr/bin/python
import io
import os
import sys
import json
import base64
from typing import IO
from PIL import Image
import cv2
import numpy as np
import PIL.Image
import easyocr
import matplotlib.pyplot as plt
from easyocr import Reader
import argparse
import pytesseract
from pytesseract import image_to_string

#!/usr/bin/python
import io
import os
import sys
import json
import base64
from typing import IO
from PIL import Image
import cv2
import numpy as np
import PIL.Image

import pytesseract
from pytesseract import image_to_string

import spacy
import en_core_web_sm
#import en_core_web_trf
import en_core_web_lg
import en_core_web_md
import nltk
from nltk.tokenize import WordPunctTokenizer
from nltk import word_tokenize
from nltk.tokenize import regexp_tokenize, wordpunct_tokenize, blankline_tokenize
from nltk.corpus import stopwords
from unidecode import unidecode
import string
import re
#from yaml.tokens import Token

nlp_sm = en_core_web_sm.load()
nlp_lg = en_core_web_lg.load()
nlp_md = en_core_web_md.load()


def read_in():
    lines = sys.stdin.readlines()
    # Since our input would only be having one line, parse our JSON data from that
    return lines


def stringToRGB(base64_string):
    imgdata = base64.b64decode(str(base64_string))
    image = Image.open(io.BytesIO(imgdata))
    img = cv2.cvtColor(np.array(image), cv2.COLOR_BGR2RGB)
    #flt = cv2.adaptiveThreshold(img,
    #                        100, cv2.ADAPTIVE_THRESH_MEAN_C,
     #                       cv2.THRESH_BINARY, 15, 16)
    return img

def improve(img):
    basewidth = 300
    
# determining the height ratio
    wpercent = (basewidth/float(img.size[0]))
    hsize = int((float(img.size[1])*float(wpercent)))
# resize image and save
    img = img.resize((basewidth,hsize), Image.ANTIALIAS)
    return img


def pre_process(corpus):
    corpus = corpus.lower()
    corpus=corpus.replace('eur', '')
 
    stopset = stopwords.words('french') + list(string.punctuation)
    corpus = " ".join([i for i in word_tokenize(corpus) if i not in stopset])
    corpus = unidecode(corpus)
    return corpus
def tva_pre_process(corpus):
    
    corpus=corpus.replace('twa', 'tva')
    corpus=corpus.replace('twaa', 'tva')
    corpus=corpus.replace('1wa', 'tva')
    corpus=corpus.replace('1va', 'tva')
    corpus=corpus.replace('tY=a', 'tva')
    corpus=corpus.replace('tvaa', 'tva')
    corpus=corpus.replace(' wa ', ' tva ')   
    return corpus
def is_number(string):
    try:
        float(string)
        return True
    except ValueError:
        return False

def find_montant_ttc(new_lines):
     
     total_ttc__nlp = nlp_md("total")
     total_ttc__nlp1 = nlp_md("total ttc")
     total_ttc__nlp2 = nlp_md("a payer")
     
     top_score = 0.7
     total_line = ''
     total_line1=[]
        #print("\n",the_line, total_nlp.similarity(the_line_nlp))
     for the_line in new_lines:
        the_line=the_line.replace(',', '.')
        the_line = word_tokenize(the_line)
        number_table = []  
        s=0        
        for l in the_line :
            l_nlp = nlp_md(l)
            if(is_number(l) == True ):
               number_table.append(l)
            s0 = total_ttc__nlp.similarity(l_nlp)
            s1 = total_ttc__nlp1.similarity(l_nlp)
            s2 = total_ttc__nlp2.similarity(l_nlp)
            tab = [ s0, s1, s2]
            s=tab[0]
            for i in [1,2]:               
               if(tab[i]>s):
                 s=tab[i]                 
            if (s > top_score ):
                  
                  total_line1.append(the_line)
         
     #print("total line1: ",total_line1)
     top_score1=0
     for h in total_line1:
         for j in h:
               l_nlp = nlp_md(j)
               s00 = total_ttc__nlp.similarity(l_nlp)
               s10 = total_ttc__nlp1.similarity(l_nlp)
               s20 = total_ttc__nlp2.similarity(l_nlp)
               tab = [ s00, s10, s20]
               s0=tab[0]
               for i in [1,2]:               
                 if(tab[i]>s0):
                    s0=tab[i]
                               
               if (s0 >= top_score1 ):
                  top_score1=s0
                  #print("top_score1: ",top_score1, h)
                  
                  total_line=h
               
                  
     #print("total line: ",top_score1)
     total_line_number = []
     for l in total_line:
         
         if(is_number(l) == True ):
                total_line_number.append(l)
         
               
     #print("total line number",total_line_number) 
     
     #print("total number",total_table)
     r=0 
     b=0
     if(len(total_line_number) == 0):
         r=0
     else :
         for i in total_line_number :
             if(float(i) > float(b)):
                 b=i
         r=float(b)

     return r

def find_montant_tva(new_lines):
     total_tva__nlp = nlp_md("tva")
     total_tva__nlp1 = nlp_md("montant tva")
     total_tva__nlp2 = nlp_md("montant.tva")
     
     top_score = 0.9
     my_line = ''
     tva_line = []
     for the_line in new_lines:
        the_line=the_line.replace(',', '.')
        the_line = word_tokenize(the_line)
        number_table = []
        s=0        
        for l in the_line :
            l_nlp = nlp_md(l)
            if(is_number(l) == True ):
               number_table.append(l)
            s0 = total_tva__nlp.similarity(l_nlp)
            s1 = total_tva__nlp1.similarity(l_nlp)
            s2 = total_tva__nlp2.similarity(l_nlp)
            tab = [ s0, s1, s2]
            s=tab[0]
            for i in [1,2]:               
               if(tab[i]>s):
                 s=tab[i]                 
            if (s > top_score ):                
                  tva_line.append(the_line)   
        
    
     #print("tva line : ",tva_line)
     tva_line_number = []
     for l in tva_line:
         k=0
         for p in l:
            if(is_number(p) == True ):
                k=k+1
         if(k>0):
             tva_line_number.append(l)  
     #print("tva line number",tva_line_number) 
     tva_line_table = '' 
     q=1000000000
     for i in tva_line_number:
         for j in i:
           if(is_number(j) == True ):
               if(float(j) <= q ):
                   q=float(j)
                   tva_line_table=i
              
     #print("tva line table",tva_line_table)
     tva_number=[]
     for token in tva_line_table:
         if(is_number(token) == True ):
               tva_number.append(token)
     
     if(len(tva_number) > 0) :
         r = tva_number[len(tva_number) - 1]
     else :
         r = 0 
   
     return r

def main():
    lines = read_in()
    #print(lines)
    window_name = 'Image'
    image = stringToRGB(lines)
    imS1 = cv2.resize(image, (700, 950))
    imS = cv2.resize(image, (800, 1000))
    pytesseract.pytesseract.tesseract_cmd = r"C:\Program Files\Tesseract-OCR\tesseract.exe"
    #pytesseract.pytesseract.tesseract_cmd = r".\Tesseract-OCR\tesseract.exe"
    #pytesseract.pytesseract.tesseract_cmd = r"/usr/local/Cellar/tesseract/4.1.1/bin/tesseract"

    image_to_text = pytesseract.image_to_string(imS1, lang='eng')
    name = sys.argv[1]
    name = name[7:-4]
    f = open('%s.txt' % name, "w")
    f.write(image_to_text)
    f.close()

    f = open('%s.txt' % name, "r")
    lines = f.readlines()
    f.close()
    new_lines = []
    
    for line in lines :
    #print("#",line)
       new_line = pre_process(line)
       new_line = tva_pre_process(new_line)
       new_lines.append(new_line)
    
    file_content = open('%s.txt' % name).read()
    file_content= file_content.lower()
    text_image = nltk.word_tokenize(file_content)  
    
    #os.remove('%s.txt' % name)
    
    montant_ttc = find_montant_ttc(new_lines)
    montant_tva = find_montant_tva(new_lines)
    #print(type(montant_ttc))
    #print(type(montant_tva))
    if(float(montant_tva)>=montant_ttc):
        montant_tva='0'
    #montant=montant.replace('€', '')
        
     #cv2.imshow(window_name, imS)
     #cv2.waitKey(0)  
     #cv2.destroyAllWindows()
    #print(sys.argv[1])
    restauration = """tél tel france avenue adresse date prix restaurant café buvette cabaret auberge hôtellerie bar hôtel bistrot restauration fast-food grill-room rôtisserie
self-service crèmerie grillade pizzeria menu terrasse magasin salle discothèque cuisinier brasserie addition 
pizza gastronomie boulangerie restaurateur mcdonald's garçon chef crêperie buffet carte snack resto poulet grill
McDo végétarien serveur déjeuner casse-croûte menu taverne dîner routier traiteur pub restoroute bouillon gastronomique articles 
palace restau sommelier table serveuse réservation snack-bar resto-bar cantine aire de restauration asiatique chinois nombre
dessert hamburger plat du jour sushi pâtissier tapas vietnamien guinguette mcdo réfectoire huîtres manger Burger King
assiette clients boissons bouillabaisse friterie grec quick cafetière caféine tasse chocolat expresso thé crème jus moulu
lait cacao comptoir noisette boire grain arôme décaféiné café au lait tiramisu vanille cappuccino latte cannelle 
cerise croissant tartine biscuit caramel chaude muffin petit noir Starbucks bière irish coffee noix cafetard leffee
Nespresso banane café filtre café-concert canne à sucre coffee liquide crevettes ananas espresso rhum Nestlé café noir riz
épice gamelle macchiato pousse-café repas frappé aromatique confiture gobelet saveur petit-déjeuner soupe café américain
capuccino jus de chaussette Nescafé glace glaces sauce raisin citron sirop formule vinaigre orange fruit limonade pulpe tomate fermenté pressé
pamplemousse betterave grenadine soda sodas huile abricot ail marinade oignon persil jus de fruit bouillir citronnade grenade 
litchi grappe mandarine canette jus de pomme jus d'orange cocktail framboise poire coriandre fermentation gingembre mangue 
volaille concombre citron vert piment légumes mélange cola salade sel mayonnaise olive viande veau beurre carottes datte 
bœuf cacahuète yaourt smoothie huile d'olive maté thé vert menthe  """

    transports = """transports transporteur logistique routier autobus autocar déplacement ferroviaire véhicule circulation
tramway voiture taxi train métro avion transport en commun aérien bateau camion automobile ratp sncf
bus navette tarif terminus vélo passager port autoroute tgv aéroport gare fourgon minibus billetterie
van voyage carburant ticket vol route tram garage aller-retour essence gasoil plomb stationnement parking
souterrain payant péage aire de stationnement location"""

    commerce = """cmagasin supermarché hypermarché épicerie caissier casino supérette carrefour auchan centre commercial leclerc aldi
alimentaire bio surface produit caisse Intermarché surgelé danette danone vanille chocolat dessert saveur crème
lait fermenté parfum cacao café boisson alcool bière vin limonade jus thé eau gazeuse sirop citron  sucre coca-cola 
gingembre vodka cannelle pepsi soja poivre safran miel menthe moutarde riz pattes poulet vinaigre concombre brocoli
poire huile pomme saucisses velouté anchois herbes sel mayonnaise olive viande veau beurre carottes datte mangue 
volaille framboise orange litchi grappe mandarine oignon persil abricot ail pamplemousse betterave banane petit pain croissant 
au raisins nutella fraise œufs cerise biscuit chips tomate"""

    hebergement = """hebergement gîte hôtellerie hôtel auberge location lit double hôtelier hôtelière 
logement séjours meublé abri airbnb camping réservation chalet réception résidence"""


    restauration = word_tokenize(restauration)
    transports = word_tokenize(transports)
    commerce = word_tokenize(commerce)
    hebergement = word_tokenize(hebergement)
    #print (text_image)
    #print(restauration)
    restau_table=[]
    transport_table=[]
    commerce_table=[]
    hebergement_table=[]

    for image_token in text_image :
         k1=0
         k2=0
         k3=0
         k4=0
         a_nlp= nlp_sm(image_token)

         for restau_token in restauration :             
             b1_nlp= nlp_sm(restau_token) 
             tmp1 = a_nlp.similarity(b1_nlp)
             if(tmp1 > k1):
                 k1=tmp1
         restau_table.append(k1)

         for transport_token in transports :             
             b2_nlp= nlp_sm(transport_token) 
             tmp2 = a_nlp.similarity(b2_nlp)
             if(tmp2 > k2):
                 k2=tmp2
         transport_table.append(k2)

         for commerce_token in commerce :             
             b3_nlp= nlp_sm(commerce_token) 
             tmp3 = a_nlp.similarity(b3_nlp)
             if(tmp3 > k3):
                 k3=tmp3
         commerce_table.append(k3)

         for hebergement_token in hebergement :             
             b4_nlp= nlp_sm(hebergement_token) 
             tmp4 = a_nlp.similarity(b4_nlp)
             if(tmp4 > k4):
                 k4=tmp4        
         hebergement_table.append(k4)
       
    for i in range(0, len(restau_table)):
        if(restau_table[i] == 1.0) :
            restau_table[i] = restau_table[i]+1
        elif ( restau_table[i]>= 0.95) :
            restau_table[i] = restau_table[i]+0.7
        elif (restau_table[i]>= 0.90) :
            restau_table[i] = restau_table[i]+0.5
    
    for i in range(0, len(transport_table)):
        if(transport_table[i] == 1.0) :
            transport_table[i] = transport_table[i]+1
        elif ( transport_table[i]>= 0.95) :
            transport_table[i] = transport_table[i]+0.7
        elif (transport_table[i]>= 0.90) :
            transport_table[i] = transport_table[i]+0.5

    for i in range(0, len(commerce_table)):
        if(commerce_table[i] == 1.0) :
            commerce_table[i] = commerce_table[i]+1
        elif ( commerce_table[i]>= 0.95) :
            commerce_table[i] = commerce_table[i]+0.7
        elif (commerce_table[i]>= 0.90) :
            commerce_table[i] = commerce_table[i]+0.5

    for i in range(0, len(hebergement_table)):
        if(hebergement_table[i] == 1.0) :
            hebergement_table[i] = hebergement_table[i]+1
        elif ( hebergement_table[i]>= 0.95) :
            hebergement_table[i] = hebergement_table[i]+0.7
        elif (hebergement_table[i]>= 0.90) :
            hebergement_table[i] = hebergement_table[i]+0.5
    restau_taux = 0
    transport_taux = 0
    commerce_taux = 0
    hebergement_taux = 0

    for i in range(0, len(restau_table)):
        restau_taux = restau_taux + restau_table[i]
    for i in range(0, len(transport_table)):
        transport_taux = transport_taux + transport_table[i]
    for i in range(0, len(commerce_table)):
        commerce_taux = commerce_taux + commerce_table[i]
    for i in range(0, len(hebergement_table)):
        hebergement_taux = hebergement_taux + hebergement_table[i]

    taux_table = []
    categorie_table=[]

    restau_taux = (restau_taux)/len(restau_table)
    taux_table.append(restau_taux)
    categorie_table.append("restauration")

    transport_taux = (transport_taux)/len(transport_table)
    taux_table.append(transport_taux)
    categorie_table.append("transports")

    commerce_taux = (commerce_taux)/len(commerce_table)
    taux_table.append(commerce_taux)
    categorie_table.append("commerce")

    hebergement_taux = (hebergement_taux)/len(hebergement_table)
    taux_table.append(hebergement_taux)
    categorie_table.append("hebergement")

    #print(restau_taux)
    #print(transport_taux)
    #print(commerce_taux)
    #print(hebergement_taux)
    
    taux_final = 0
    cat=''
    for i in range(0,4):
        if( taux_table[i] > taux_final):
            taux_final = taux_table[i]
            cat= categorie_table[i]
       
    categorie = ''  
    if(taux_final >= 0.65):
        categorie = cat
        
    else :
        categorie = "autre"


    print(montant_ttc)
    print(montant_tva)   
    print(categorie)

if __name__ == "__main__":
    main()