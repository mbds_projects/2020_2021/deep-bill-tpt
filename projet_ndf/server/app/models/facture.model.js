const mongoose = require('mongoose');

const factureSchema = new mongoose.Schema({
    userId: {
        type: String,
        trim: true,
        required: true
    },
    username: {
        type: String,
        trim: true
    },
    name: {
        type: String,
        trim: true,
        required: true
    },
    image: {
        type: String,
        trim: true,
        required: true
    },
    montant: {
        type: Number,
        trim: true,

    },
    tva: {
        type: Number,
        trim: true,

    },
    montant_ht: {
        type: Number,
        trim: true,

    },
    traitee: {
        type: Boolean,
        trim: true
    },
    categories: 
        {

            type: String,
            trim: true

        }
      
}, {
    timestamps: true
}
);

module.exports = mongoose.model('Facture', factureSchema);