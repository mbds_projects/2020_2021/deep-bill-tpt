const mongoose = require("mongoose");

const Categorie = mongoose.model(
  "categorie",
  new mongoose.Schema({
    catégorie: String
  })
);

module.exports = Categorie;
